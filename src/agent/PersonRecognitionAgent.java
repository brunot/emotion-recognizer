/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agent;

/**
 *
 * @author Rafael
 */
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import facedetection.detectedFace;
import facerecognition.FaceRecognition;
import facerecognition.recognizedPerson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PersonRecognitionAgent extends Agent {
	String nickname = "PersonRecognitionAgent";
	AID id = new AID(nickname, AID.ISGUID);
	FaceRecognition faceRecognition = new FaceRecognition();
	IplImage frame = null;
	int aux = 0;
	Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());

	protected void setup() {

		ACLMessage msgTrain = new ACLMessage(ACLMessage.INFORM); 
		msgTrain.addReceiver(new AID("TrainPersonRecognitionAgent", AID.ISLOCALNAME));
		msgTrain.setContent(""+faceRecognition.faceRecognitionIO.getnPersons());
		send(msgTrain);

		addBehaviour(new CyclicBehaviour (this) {
			public void action () {
				ACLMessage msg = myAgent.receive();
				if (msg != null) {
					
					if(msg.getSender().getLocalName().compareTo("DetectionAgent")==0){
						frame = cvLoadImage("resources/frame.jpg");
						ArrayList<detectedFace> detected = new ArrayList<detectedFace>();
						try {
							detected = (ArrayList<detectedFace>) msg.getContentObject();
						} catch (UnreadableException ex) {
							Logger.getLogger(MonitorAgent.class.getName()).log(Level.SEVERE, null, ex);
						}
						ArrayList<recognizedPerson> recognized = new ArrayList<recognizedPerson>();
						logger.log(jade.util.Logger.INFO,getLocalName()+":: == "+detected.size()+" Deteccoes recebidas <-"
								+ " from "+msg.getSender().getLocalName());
						if (frame != null) {
							recognized = faceRecognition.recognizeFaces(detected, frame);
							ACLMessage msgSend = new ACLMessage(ACLMessage.INFORM); 
							msgSend.addReceiver(new AID("MonitorAgent", AID.ISLOCALNAME));
							msgSend.addReceiver(new AID("InstantEmotionRecognitionAgent", AID.ISLOCALNAME));
							if (!detected.isEmpty()) {
								try {
									msgSend.setContentObject(recognized);
								} catch (IOException ex) {
									Logger.getLogger(PersonRecognitionAgent.class.getName()).log(Level.SEVERE, null, ex);
								}
								send(msgSend);
							}
							cvReleaseImage(frame);
							recognized.clear();
							detected.clear();
							recognized = null;
							detected = null;
						}
					}
					if(msg.getSender().getLocalName().compareTo("TrainPersonRecognitionAgent")==0){
						logger.log(jade.util.Logger.INFO,"------------------------Reseting---------------------------");
						faceRecognition.faceRecognitionIO.loadTrainingData();
					}
					

				}
				else{
					block();
				}
			}

		});

	}
}