package agent;

import java.util.logging.Logger;

import emotionrecognition.InstantEmotionRecognitionIO;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

public class TrainEmotionRecognitionAgent extends Agent {
	String nickname = "TrainEmotionRecognitionAgent";
	AID id = new AID(nickname, AID.ISGUID);
	Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());

	protected void setup() {
		addBehaviour(new CyclicBehaviour (this) {
			public void action () {
				ACLMessage receivedMessage = myAgent.receive();
				if (receivedMessage != null) {
					    InstantEmotionRecognitionIO.train(receivedMessage.getContent());
						ACLMessage resetEmotionRecognizer = new ACLMessage(ACLMessage.REQUEST);
						resetEmotionRecognizer.addReceiver(new AID("InstantEmotionRecognitionAgent", AID.ISLOCALNAME));
						myAgent.send(resetEmotionRecognizer);
					
				}
				else{
					block();
				}
			}
		});
	}
}
