package agent;

import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.googlecode.javacv.cpp.opencv_core.IplImage;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import emotionrecognition.InstantEmotionRecognition;
import emotionrecognition.recognizedEmotion;
import facerecognition.recognizedPerson;

@SuppressWarnings("serial")
public class InstantEmotionRecognitionAgent extends Agent {
	String nickname = "InstantEmotionRecognitionAgent";
	AID id = new AID(nickname, AID.ISGUID);
	InstantEmotionRecognition emotionDetect = new InstantEmotionRecognition();
	IplImage frame = null;
    Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());
    ArrayList<recognizedPerson> recognizedPerson = null;
    ArrayList<recognizedEmotion> recognizedEmotion = null;
	
    ACLMessage msg = null, msgSend = null;
    
	protected void setup(){
		
		addBehaviour(new CyclicBehaviour(this) {
			
			@SuppressWarnings("unchecked")
			public void action() {
	            msg = myAgent.receive();
	            if ( msg != null ) {
	            	if(msg.getPerformative() == ACLMessage.INFORM){
	            		frame = cvLoadImage("resources/frame.jpg");
	            		recognizedPerson = new ArrayList<recognizedPerson>();
	            		try {
	            			recognizedPerson = (ArrayList<recognizedPerson>) msg.getContentObject();
	            		} catch (UnreadableException ex) {
	            			Logger.getLogger(MonitorAgent.class.getName()).log(Level.SEVERE, null, ex);
	            		}
	            		recognizedEmotion = new ArrayList<recognizedEmotion>();
	            		logger.log(jade.util.Logger.INFO,getLocalName()+":: == "+recognizedPerson.size()+" Deteccoes recebidas <-"
	            				+ " from "+msg.getSender().getLocalName());
	            		if( frame != null ){
	            			recognizedEmotion = emotionDetect.RecognizeEmotionOnFaceList(recognizedPerson, frame);
	            			msgSend = new ACLMessage(ACLMessage.INFORM); 
	            			msgSend.addReceiver(new AID("ContinuousEmotionRecognitionAgent", AID.ISLOCALNAME));
	            			if (!recognizedPerson.isEmpty()) {
	            				try {
	            					msgSend.setContentObject(recognizedEmotion);
	            				} catch (IOException ex) {
	            					Logger.getLogger(PersonRecognitionAgent.class.getName()).log(Level.SEVERE, null, ex);
	            				}
	            				send(msgSend);
	            			}
	            			cvReleaseImage(frame);
	            		}
	            	}
	            	else if(msg.getPerformative() == ACLMessage.REQUEST){
	            		emotionDetect = new InstantEmotionRecognition();
	            		logger.log(jade.util.Logger.INFO, "Emotion training finished!");
	            	}
	            }
	            else{
	            	block();
	            }
			}
			
		});
	}
	
}
