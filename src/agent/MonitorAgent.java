/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agent;

/**
 *
 * @author Rafael
 */
import com.googlecode.javacv.cpp.opencv_core.CvPoint;
import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import static com.googlecode.javacv.cpp.opencv_core.CV_AA;
import static com.googlecode.javacv.cpp.opencv_core.cvFont;
import static com.googlecode.javacv.cpp.opencv_core.cvPoint;
import static com.googlecode.javacv.cpp.opencv_core.cvPutText;
import static com.googlecode.javacv.cpp.opencv_core.cvRectangle;
import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import facedetection.detectedFace;
import facerecognition.FaceRecognition;
import facerecognition.recognizedPerson;
import emotionrecognition.particle;
import emotionrecognition.recognizedEmotion;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import userInterface.UserInterface;

public class MonitorAgent extends Agent {
	private final int EMOTION_INFORMATION = 0;
	private final int PARTICLE_INFORMATION = 1;
	private final static int ADD_NEW_PERSON = 1;
	private final static int RETRAIN_ALL = 2;
    private static final int ADDWEBCAMBEHAVIOUR = 10;
    private static final int ADDVIDEOBEHAVIOUR = 11;
	private final static int RESET = 90;


	String nickname = "MonitorAgent";
	AID id = new AID(nickname, AID.ISGUID);
	IplImage frame = null;
	ArrayList<detectedFace> detectedFace = new ArrayList<detectedFace>();
	ArrayList<recognizedPerson> recognizedPerson = new ArrayList<recognizedPerson>();
	List<ArrayList<recognizedEmotion>> recognizedEmotion = new ArrayList<ArrayList<recognizedEmotion>>();
	int NOREC = 0;
	Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());

	UserInterface tv = new UserInterface(this);

	//Rectangle Parameters
	detectedFace firstFaceRectangle = new detectedFace();
	detectedFace secondFaceRectangle = new detectedFace();
	
	// Person
	recognizedPerson firstPerson = new recognizedPerson();
	recognizedPerson secondPerson = new recognizedPerson();

	// Particle
	List<ArrayList<particle>> particleMotion = new ArrayList<ArrayList<particle>>();

	protected void setup() {

		tv.selectImageAgentInput();
		
		addBehaviour(new CyclicBehaviour (this) {
			@SuppressWarnings("unchecked")
			public void action () {
				ACLMessage msg = myAgent.receive();
				if (msg != null) {
					frame = cvLoadImage("resources/frame.jpg");

					switch (msg.getSender().getLocalName()) {

					case ("DetectionAgent"):
						try {
							detectedFace = (ArrayList<detectedFace>) msg.getContentObject();
						} catch (UnreadableException ex) {
							Logger.getLogger(MonitorAgent.class.getName()).log(Level.SEVERE, null, ex);
						}
					logger.log(jade.util.Logger.INFO,getLocalName()+":: == Frame "+"recebida <-"
							+ " from "+msg.getSender().getLocalName());
					logger.log(jade.util.Logger.INFO,"Pessoas detectadas: "+detectedFace.size());
					NOREC=0;
					break;

					case ("PersonRecognitionAgent"):
						try {
							recognizedPerson = (ArrayList<recognizedPerson>) msg.getContentObject();
						} catch (UnreadableException ex) {
							Logger.getLogger(MonitorAgent.class.getName()).log(Level.SEVERE, null, ex);
						}
					logger.log(jade.util.Logger.INFO,getLocalName()+":: == Frame "+"recebida <-"
							+ " from "+msg.getSender().getLocalName());
					logger.log(jade.util.Logger.INFO,"Pessoas reconhecidas: "+recognizedPerson.size());
					NOREC=0;
					break;

					case ("ContinuousEmotionRecognitionAgent"):
						if(msg.getPerformative() == EMOTION_INFORMATION){
							try {
								recognizedEmotion = (List<ArrayList<emotionrecognition.recognizedEmotion>>) msg.getContentObject();
							} catch (UnreadableException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							logger.log(jade.util.Logger.INFO,getLocalName()+":: == Frame "+"recebida <-"
									+ " from "+msg.getSender().getLocalName());
							refreshCharts(recognizedEmotion);
						}
						else if (msg.getPerformative() == PARTICLE_INFORMATION){
							try {
								particleMotion = (List<ArrayList<particle>>) msg.getContentObject();
							} catch (UnreadableException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							logger.log(jade.util.Logger.INFO,getLocalName()+":: == Frame "+"recebida <-"
									+ " from "+msg.getSender().getLocalName());
							refreshParticleChart(particleMotion);
						}
					//					displayEmotions(recognizedEmotion.get(recognizedEmotion.size()-1).getAllDistanceBase100());

					break;

					case ("ImageAgent"):
						NOREC++;
					break;

					}
					if (!detectedFace.isEmpty()){
						firstFaceRectangle = detectedFace.get(0);
						if(detectedFace.size() > 1){
							secondFaceRectangle = detectedFace.get(1);
						}
					}
					cvRectangle(
							frame,
							cvPoint(firstFaceRectangle.getXinitialPoint(), firstFaceRectangle.getYinitialPoint()),
							cvPoint(firstFaceRectangle.getXfinalPoint(), firstFaceRectangle.getYfinalPoint()), CvScalar.GREEN, 2, CV_AA, 0);
					if(secondFaceRectangle.getTimestamp() != 0){
						cvRectangle(
								frame,
								cvPoint(firstFaceRectangle.getXinitialPoint(), firstFaceRectangle.getYinitialPoint()),
								cvPoint(firstFaceRectangle.getXfinalPoint(), firstFaceRectangle.getYfinalPoint()), CvScalar.GREEN, 2, CV_AA, 0);
					}
					if (!recognizedPerson.isEmpty()){
						firstPerson = recognizedPerson.get(0);
						if(recognizedPerson.size() > 1)
							secondPerson = recognizedPerson.get(1);
					}
					if (firstPerson.getConfidence() > 0.55) {
						cvPutText(frame, firstPerson.getName(),
								new CvPoint(firstPerson.getXpontoNome(),
										firstPerson.getYpontoNome()),
										cvFont(2, 2), CvScalar.BLUE);
					} else {
						cvPutText(frame, "Unknown", new CvPoint(firstPerson
								.getXpontoNome(), firstPerson
								.getYpontoNome()), cvFont(1, 1), CvScalar.BLUE);
					}
					
					if (secondPerson.getConfidence() > 0.55) {
						cvPutText(frame, secondPerson.getName(),
								new CvPoint(secondPerson.getXpontoNome(),
										secondPerson.getYpontoNome()),
										cvFont(2, 2), CvScalar.BLUE);
					} else {
						cvPutText(frame, "Unknown", new CvPoint(secondPerson
								.getXpontoNome(), secondPerson
								.getYpontoNome()), cvFont(1, 1), CvScalar.BLUE);
					}
					
					if (!detectedFace.isEmpty() && !recognizedPerson.isEmpty() && !recognizedEmotion.isEmpty()) {
						if (detectedFace.size() == recognizedPerson.size() && 
								detectedFace.size() == recognizedEmotion.size()){
							java.util.Date date = new java.util.Date();
							logger.log(jade.util.Logger.INFO,"---------------------------------- MonitorAgent TIMEend: "+new Timestamp(date.getTime()));
							//	                        frame = tv.processDR(detectedFace, recognizedPerson, recognizedEmotion, frame);
						}
						if (frame!=null) {
							displayFrame(frame, recognizedPerson.get(0).getName());
							detectedFace = new ArrayList<detectedFace>();
							recognizedPerson = new ArrayList<recognizedPerson>();
						}
					}
					else {
						displayFrame(frame, "");
					}
				}
				else {
					block();
				}
			}
		});
	}

	private void displayFrame(final IplImage frame, final String name){
		Runnable addFrame = new Runnable(){
			public void run(){
				tv.displayImage(frame);
//				tv.displayPersonName(name);
//				cvReleaseImage(frame);
			}
		};
		SwingUtilities.invokeLater(addFrame);
	}

	private void refreshCharts(final List<ArrayList<emotionrecognition.recognizedEmotion>> recognizedEmotion){
		Runnable displayEmotions = new Runnable(){
			public void run(){
				tv.refreshEmotionCharts(recognizedEmotion);
			}
		};
		SwingUtilities.invokeLater(displayEmotions);
	}

	private void refreshParticleChart(final List<ArrayList<particle>> particleMotion){
		Runnable displayParticle = new Runnable(){
			public void run(){
				tv.refreshParticleChart(particleMotion);
			}
		};
		SwingUtilities.invokeLater(displayParticle);
	}

	public void trainPersonRecognition(){
		logger.log(jade.util.Logger.INFO,"Start training");
		java.util.Date date = new java.util.Date();
		// Pega o nome da pessoa a ser adicionada no banco
		String name = JOptionPane.showInputDialog("Insira o nome da pessoa a ser adicionada ao banco:");   
		logger.log(jade.util.Logger.INFO,"---------------------------------- TrainPersonRecognitionAgent TIMEstart : " + new Timestamp(date.getTime()));

		ACLMessage msg = new ACLMessage(ACLMessage.INFORM); 
		msg.addReceiver(new AID("TrainPersonRecognitionAgent", AID.ISLOCALNAME));
		msg.setContent(name);
		msg.setPerformative(ADD_NEW_PERSON);
		send(msg);
	}
	
	public void trainPersonRecognition(String filename){
		String extension;
		//This is where a real application would open the file.
		logger.log(jade.util.Logger.INFO, "Got filename: " + filename);
		if (filename.lastIndexOf('.') > 0){
			extension = filename.substring(filename.lastIndexOf('.') + 1);
			System.out.println(extension);
			if (extension.equals("txt")){
				ACLMessage msg = new ACLMessage(ACLMessage.INFORM); 
				msg.addReceiver(new AID("TrainPersonRecognitionAgent", AID.ISLOCALNAME));
				msg.setContent(filename);
				msg.setPerformative(RETRAIN_ALL);
				send(msg);
			}
			else {
				logger.log(jade.util.Logger.INFO, "Please, inform a txt file");
			}
		}
		else {
			logger.log(jade.util.Logger.INFO, "Could not parse file extension");
		}
	}

	public void trainEmotionRecognition(String filename){
		String extension;
		//This is where a real application would open the file.
		logger.log(jade.util.Logger.INFO, "Got filename: " + filename);
		if (filename.lastIndexOf('.') > 0){
			extension = filename.substring(filename.lastIndexOf('.') + 1);
			System.out.println(extension);
			if (extension.equals("csv")){
				ACLMessage msg = new ACLMessage(ACLMessage.INFORM); 
				msg.addReceiver(new AID("TrainEmotionRecognitionAgent", AID.ISLOCALNAME));
				msg.setContent(filename);
				send(msg);
			}
			else {
				logger.log(jade.util.Logger.INFO, "Please, inform a csv file");
			}
		}
		else {
			logger.log(jade.util.Logger.INFO, "Could not parse file extension");
		}
	}

	public void takeScreenshot(){
		logger.log(jade.util.Logger.INFO,"Shoot frame");

		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST); 
		msg.addReceiver(new AID("ImageAgent", AID.ISLOCALNAME));
		msg.setContent("SCREENSHOT");
		send(msg);
	}
	
	public void resetAll(){
		logger.log(jade.util.Logger.INFO,"Reset");
		
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST); 
		msg.addReceiver(new AID("ContinuousEmotionRecognitionAgent", AID.ISLOCALNAME));
		msg.setPerformative(RESET);
		send(msg);
		
	}
	
	public void selectImageAgentBehaviour(String filename){
		if(filename == ""){
			ACLMessage msg = new ACLMessage(ADDWEBCAMBEHAVIOUR); 
			msg.addReceiver(new AID("ImageAgent", AID.ISLOCALNAME));
			send(msg);
		}
		else {
			ACLMessage msg = new ACLMessage(ADDVIDEOBEHAVIOUR); 
			msg.addReceiver(new AID("ImageAgent", AID.ISLOCALNAME));
			msg.setContent(filename);
			send(msg);
		}
		
	}

}