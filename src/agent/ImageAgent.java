/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agent;

/**
 *
 * @author Rafael
 */
import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvSaveImage;
import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import facedetection.DetectFaces;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import training.Trainer;
import utilities.CameraGrabber;

@SuppressWarnings("serial")
public class ImageAgent extends Agent {
    String nickname = "ImageAgent";
    AID id = new AID(nickname, AID.ISGUID);
    DetectFaces faceDetector = new DetectFaces();
    CameraGrabber cameraGrabber = new CameraGrabber();
    IplImage frame = null;
    String date = null;
    String name = null;
    int i = 0;
    private TickerBehaviour ticker = null;
    private OneShotBehaviour videoAnalyser = null;
    private CyclicBehaviour messageReceiver = null;
    
    // Constants
    private static int INITIALSETUPDELAY = 3000;
    private static int FRAMEGRABBERDELAY = 333;

    Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());
    
    ACLMessage msg = null,saveTrainingImagesResponse = null;
    
    private static final int ADDWEBCAMBEHAVIOUR = 10;
    private static final int ADDVIDEOBEHAVIOUR = 11;
    
    private String filename;
    
    protected void setup() {
        
    	//Delay for initial setup
        doWait(INITIALSETUPDELAY);
        
        //Avoid printing logger messages on console
        LogManager.getLogManager().reset();
        
        //Ticker Behaviour
        ticker = new TickerBehaviour (this, FRAMEGRABBERDELAY) {
            @Override
            protected void onTick () {
                java.util.Date date= new java.util.Date();
                logger.log(jade.util.Logger.INFO,"---------------------------------- ImageAgent TIMEstart "+i+" : "+new Timestamp(date.getTime()));
                frame = cameraGrabber.grabFrameFromCamera();
                
                cvSaveImage("resources/frame.jpg", frame);
                cvReleaseImage(frame);
                System.gc();
                msg = new ACLMessage(ACLMessage.INFORM); 
                msg.addReceiver(new AID("DetectionAgent", AID.ISLOCALNAME));
                msg.addReceiver(new AID("MonitorAgent", AID.ISLOCALNAME));
                msg.setContent(""+System.currentTimeMillis());
                send(msg);
                i++;
         
            }
        };
        
        //Ticker Behaviour
        videoAnalyser = new OneShotBehaviour (this) {
            @Override
			public void action () {
                java.util.Date date= new java.util.Date();
                logger.log(jade.util.Logger.INFO,"---------------------------------- ImageAgent TIMEstart "+i+" : "+new Timestamp(date.getTime()));
                
        		FrameGrabber grabber = new OpenCVFrameGrabber(filename); 
        		try {      

        			//Start grabber to capture video
        			grabber.start();      
        			int j = 0;
        			long initial_time = System.currentTimeMillis();
        			long time = initial_time;

        			while (j < grabber.getLengthInFrames()) {

        				//inser grabed video fram to IplImage img
        				frame = grabber.grab();
        				time += 1000/25;

        				if (frame != null) {
        					logger.log(jade.util.Logger.INFO, "Pegando imagem de vídeo");
        	                cvSaveImage("resources/frame.jpg", frame);
//        	                cvReleaseImage(frame);
//        	                System.gc();
        	                i++;
        	                msg = new ACLMessage(ACLMessage.INFORM); 
        	                msg.addReceiver(new AID("DetectionAgent", AID.ISLOCALNAME));
        	                msg.addReceiver(new AID("MonitorAgent", AID.ISLOCALNAME));
        	                msg.setContent("" + time);
        	                send(msg);
        				}
        				j++;
        				doWait(350);
        			}
        		}
        		catch (Exception e) {      
        		}
         
            }
        };        
        
        messageReceiver = new CyclicBehaviour (this) {
            public void action () {
            msg = myAgent.receive();
                if (msg != null) {
                	if (msg.getPerformative() == ACLMessage.REQUEST){                  
                        frame = cameraGrabber.grabFrameFromCamera();
                        java.util.Date now = new java.util.Date();
                        date = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss").format(now);
                        cvSaveImage("output/screenshots/" + date+ "-frame.jpg", frame);
                        cvReleaseImage(frame);
                        System.gc();
                        logger.log(jade.util.Logger.INFO, "Got screenshot");

                	}
                	
                	else if (msg.getPerformative() == ACLMessage.INFORM){
                		name = msg.getContent();
                		ticker.stop();
                		if (Trainer.saveDetections(name, cameraGrabber, faceDetector)){
                			logger.log(jade.util.Logger.INFO, "Got all images");
                			ticker.reset();
                		}
                		saveTrainingImagesResponse = new ACLMessage(ACLMessage.REQUEST);
                		saveTrainingImagesResponse.addReceiver(msg.getSender());
                		saveTrainingImagesResponse.setContent(name);
                		send(saveTrainingImagesResponse);
                	}
                	
                	else if (msg.getPerformative() == ADDWEBCAMBEHAVIOUR){
                		addBehaviour(ticker);
                	}
                	
                	else if (msg.getPerformative() == ADDVIDEOBEHAVIOUR) {
                		filename = msg.getContent();
                		addBehaviour(videoAnalyser);
                	}
                }
                else {
                	block();
                }
            }
        };
        
        // Add Behaviours to Agent
        addBehaviour(messageReceiver);
        
    }
}