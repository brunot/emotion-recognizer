/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agent;

import com.googlecode.javacv.cpp.opencv_core.IplImage;

import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import facedetection.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("serial")
public class DetectionAgent extends Agent {
	String nickname = "DetectionAgent";
	long currentTime;
	AID id = new AID(nickname, AID.ISGUID);
	DetectFaces faceDetector = new DetectFaces();
	ArrayList<detectedFace> detected = null;
	IplImage frame = null;
	int aux = 0;
	Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());
	ACLMessage msg = null, sendMessage = null;
	

	protected void setup() {

		addBehaviour(new CyclicBehaviour (this) {

			public void action () {
				msg = myAgent.receive();
				if (msg != null) {
					
					frame = cvLoadImage("resources/frame.jpg");
					detected = new ArrayList<detectedFace>();
					currentTime = Long.parseLong(msg.getContent());
					if (frame != null)
						detected = faceDetector.detectFaces(frame, currentTime);
					logger.log(jade.util.Logger.INFO, getLocalName()+":: == Frame "+msg.getContent()+" recebida <-"
							+ " from "+msg.getSender().getLocalName());

					//Create Message
					sendMessage = new ACLMessage(ACLMessage.INFORM); 
					sendMessage.addReceiver(new AID("PersonRecognitionAgent", AID.ISLOCALNAME));
					sendMessage.addReceiver(new AID("MonitorAgent", AID.ISLOCALNAME));

					if (!detected.isEmpty()) {
						try {
							sendMessage.setContentObject(detected);
						} catch (IOException ex) {
							Logger.getLogger(DetectionAgent.class.getName()).log(Level.SEVERE, null, ex);
						}
						send(sendMessage);
					}
					
					detected.clear();
					detected = null;
					cvReleaseImage(frame);
					
				}
				else {
					block();
				}
			}
		});

	}
}