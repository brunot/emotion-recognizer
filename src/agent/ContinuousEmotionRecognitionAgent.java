package agent;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.googlecode.javacv.cpp.opencv_core.IplImage;

import emotionrecognition.ContinuousEmotionRecognition;
import emotionrecognition.particle;
import emotionrecognition.recognizedEmotion;

@SuppressWarnings("serial")
public class ContinuousEmotionRecognitionAgent extends Agent {
	private final int EMOTION_INFORMATION = 0;
	private final int PARTICLE_INFORMATION = 1;
	private final int RESET = 90;
	
	String nickname = "ContinuousEmotionRecognitionAgent";
	AID id = new AID(nickname, AID.ISGUID);
	ContinuousEmotionRecognition emotionDetect = new ContinuousEmotionRecognition(System.currentTimeMillis());
	IplImage frame = null;
    Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());
    
    List<ArrayList<recognizedEmotion>> recognizedEmotionOverTime = new ArrayList<ArrayList<recognizedEmotion>>();
    List<ArrayList<particle>> particleMotion = new ArrayList<ArrayList<particle>>();
    
    ACLMessage msg = null, sendMessage = null;
	
	protected void setup(){
		
		addBehaviour(new CyclicBehaviour(this) {
			@SuppressWarnings("unchecked")
			public void action() {
	            msg = myAgent.receive();
	            if ( msg != null ) {
	            	if(msg.getPerformative() == ACLMessage.INFORM){
	            		
	            		logger.log(jade.util.Logger.INFO, "Continuous Emotion Recognition Agent");
	            		ArrayList<recognizedEmotion> recognizedEmotion = new ArrayList<recognizedEmotion>();
	            		try {
	            			recognizedEmotion = (ArrayList<recognizedEmotion>) msg.getContentObject();
	            			emotionDetect.createParticlesMovement(recognizedEmotion);
	            			recognizedEmotionOverTime = emotionDetect.getRecognizedInstantEmotions();
	            			particleMotion = emotionDetect.getParticle();
	    
	            			
	            			sendMessage = new ACLMessage(ACLMessage.INFORM); 
	            			sendMessage.addReceiver(new AID("MonitorAgent", AID.ISLOCALNAME));
	            			sendMessage.setContentObject((Serializable) recognizedEmotionOverTime);
	            			sendMessage.setPerformative(EMOTION_INFORMATION);
	                        send(sendMessage);
	                        
	            			sendMessage = new ACLMessage(ACLMessage.INFORM); 
	            			sendMessage.addReceiver(new AID("MonitorAgent", AID.ISLOCALNAME));
	            			sendMessage.setContentObject((Serializable) particleMotion);
	            			sendMessage.setPerformative(PARTICLE_INFORMATION);
	                        send(sendMessage);
	                        

	            		} catch (UnreadableException ex) {
	            			Logger.getLogger(MonitorAgent.class.getName()).log(Level.SEVERE, null, ex);
	            		} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
	            	}
	            	
	            	if(msg.getPerformative() == RESET){
	            		emotionDetect = new ContinuousEmotionRecognition(System.currentTimeMillis());
	            	}
	            }
	            else{
	            	block();
	            }
			}
			
		});
	}
	
}
