/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agent;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;

import facerecognition.FaceRecognition;
import training.Trainer;

/**
 *
 * @author Rafael
 */
public class TrainPersonRecognitionAgent extends Agent{
	
	final static int ADD_NEW_PERSON = 1;
	final static int RETRAIN_ALL = 2;

	String nickname = "TrainPersonRecognitionAgent";
	AID id = new AID(nickname, AID.ISGUID);
	int numberOfPersons;
	Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());

	protected void setup() {
		addBehaviour(new CyclicBehaviour (this) {
			public void action () {
				ACLMessage receivedMessage = myAgent.receive();
				if (receivedMessage != null) {
					if (receivedMessage.getSender().getLocalName().compareTo("PersonRecognitionAgent")==0) {
						numberOfPersons = (Integer.parseInt(receivedMessage.getContent()));
					}
					else if(receivedMessage.getSender().getLocalName().compareTo("MonitorAgent")==0) {
						if(receivedMessage.getPerformative() == ADD_NEW_PERSON){
						String name = receivedMessage.getContent();
						logger.log(jade.util.Logger.INFO,"Name received! Training Started for " + name);
						getNewPersonImages(name);
						}
						else if(receivedMessage.getPerformative() == RETRAIN_ALL){
						java.util.Date date= new java.util.Date();
						String filename = receivedMessage.getContent();
						FaceRecognition faceRecognition = new FaceRecognition();
						faceRecognition.learn(filename);
						ACLMessage learn_message = new ACLMessage(ACLMessage.INFORM); 
						learn_message.addReceiver(new AID("PersonRecognitionAgent", AID.ISLOCALNAME));
						try {
							learn_message.setContentObject(true);
						} catch (IOException ex) {
							Logger.getLogger(TrainPersonRecognitionAgent.class.getName()).log(Level.SEVERE, null, ex);
						}
						doWait(1000);
						send(learn_message);        
						date = new java.util.Date();
						logger.log(jade.util.Logger.INFO, "---------------------------------- TrainPersonRecognitionAgent TIMEend : "+new Timestamp(date.getTime()));
						}
					}
					else if(receivedMessage.getSender().getLocalName().compareTo("ImageAgent")==0) {
						String name = receivedMessage.getContent();
						logger.log(jade.util.Logger.INFO,"Started DB updater");
						addNewPersonToDatabase(name);
						ACLMessage msg = new ACLMessage(ACLMessage.PROPOSE); 
						msg.addReceiver(new AID("PersonRecognitionAgent", AID.ISLOCALNAME));
						send(msg);
					}
				}
				else{
					block();
				}
			}
		});
	}
	
	public void getNewPersonImages(String name) {
		// Envia mensagem ao ImageAgent
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM); 
		msg.addReceiver(new AID("ImageAgent", AID.ISLOCALNAME));
		msg.setContent(name);
		send(msg);
		logger.log(jade.util.Logger.INFO, "wait for message response!");
	}

	public void addNewPersonToDatabase(String name){
		java.util.Date date= new java.util.Date();
		// Faz o processo de treino
		Trainer.agentTraining(name, numberOfPersons+1);
		// Realiza todo o procedimento de learn
		ACLMessage learn_message = new ACLMessage(ACLMessage.INFORM); 
		learn_message.addReceiver(new AID("PersonRecognition", AID.ISLOCALNAME));
		try {
			learn_message.setContentObject(true);
		} catch (IOException ex) {
			Logger.getLogger(TrainPersonRecognitionAgent.class.getName()).log(Level.SEVERE, null, ex);
		}
		doWait(1000);
		send(learn_message);        
		date = new java.util.Date();
		logger.log(jade.util.Logger.INFO, "---------------------------------- TrainPersonRecognitionAgent TIMEend : "+new Timestamp(date.getTime()));
	}
}