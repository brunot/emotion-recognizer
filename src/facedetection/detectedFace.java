package facedetection;

public class detectedFace implements java.io.Serializable{

    private int XinitialPoint;
    private int YinitialPoint;
    private int XfinalPoint;
    private int YfinalPoint;
    private long timestamp;


    // Class Public Methods
    public int getXinitialPoint() {
        return XinitialPoint;
    }

    public void setXinitialPoint(int XinitialPoint) {
        this.XinitialPoint = XinitialPoint;
    }

    public int getYinitialPoint() {
        return YinitialPoint;
    }

    public void setYinitialPoint(int YinitialPoint) {
        this.YinitialPoint = YinitialPoint;
    }

    public int getXfinalPoint() {
        return XfinalPoint;
    }

    public void setXfinalPoint(int XfinalPoint) {
        this.XfinalPoint = XfinalPoint;
    }

    public int getYfinalPoint() {
        return YfinalPoint;
    }

    public void setYfinalPoint(int YfinalPoint) {
        this.YfinalPoint = YfinalPoint;
    }

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public int getXmiddlePoint() {
		return (XfinalPoint+XinitialPoint)/2;
	}
	
	public int getYmiddlePoint() {
		return (YfinalPoint + YinitialPoint)/2;
	}
}