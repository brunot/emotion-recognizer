package facedetection;

import java.util.ArrayList;
import java.util.logging.Logger;

import com.googlecode.javacpp.Pointer;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;

public class DetectFaces {

	// Constants
	private static final String CASCADE_FILE = "resources/haarcascades/haarcascade_frontalface_alt.xml";
	private static final int TRACK_RANGE = 50;

	// Class variables
	CvHaarClassifierCascade detectFacesHaarClassifier;
	// memoria para utilizacao do detector

	Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());

	IplImage detectImage;
	
    // Person's detected face last positions
    static detectedFace firstDetectedPerson = new detectedFace();
    static detectedFace secondDetectedPerson = new detectedFace();

	// Constructor
	public DetectFaces(){
		detectFacesHaarClassifier = new CvHaarClassifierCascade(cvLoad(CASCADE_FILE));
	}

	// Public Methods
	public CvSeq detectFace(IplImage image) {
		CvMemStorage faceDetectorStorage = cvCreateMemStorage(0);
		// Deteccao de faces em image
		CvSeq faceContainer = cvHaarDetectObjects(image, 
				detectFacesHaarClassifier, 
				faceDetectorStorage, 
				1.1, // aumenta escala de 10% a cada etapa
				3, // junta grupos de 3 deteccoes
//				CV_HAAR_DO_CANNY_PRUNING, // pula regioes improvaveis de conter uma face
				CV_HAAR_SCALE_IMAGE,
				cvSize(128, 128), // menor face a detectar
				cvSize(384, 384)); // maior face a detectar
		cvReleaseMemStorage(faceDetectorStorage);
		faceDetectorStorage.deallocate();
		return faceContainer;
	}

	public ArrayList<detectedFace> detectFaces(IplImage image, long currentTime) {
		CvMemStorage faceDetectorStorage = cvCreateMemStorage(0);
		ArrayList<detectedFace> detectedFaces = new ArrayList<detectedFace>();
		CvSeq facesContainer = null;
		CvRect faceRectangle = null;
		detectedFace person = null;
		Pointer element = null;
		int total;
		
		detectImage = cvCreateImage(cvGetSize(image),image.depth(), image.nChannels() );
		cvCopy(image, detectImage);
		// Deteccao de faces em image
		facesContainer = cvHaarDetectObjects(detectImage, 
				detectFacesHaarClassifier, 
				faceDetectorStorage, 
				1.1, // aumenta escala de 10% a cada etapa
				3, // junta grupos de 3 deteccoes
				CV_HAAR_FIND_BIGGEST_OBJECT | 
				CV_HAAR_DO_CANNY_PRUNING, // pula regioes improvaveis de conter uma face
				cvSize(40, 40), // menor face a detectar
				cvSize(300, 300)); // maior face a detectar
		logger.log(jade.util.Logger.INFO, facesContainer.toString() + "Total = " + facesContainer.total());
		if(facesContainer != null && !facesContainer.isNull()){
			total = facesContainer.total();
			for (int i = 0; i < total; i++) {
				person = new detectedFace();

				// salva pontos do retangulo em torno de cada deteccao em um ArrayList
				try{
					element = cvGetSeqElem(facesContainer, i);
					//					logger.log(jade.util.Logger.INFO, element.toString());
					faceRectangle = new CvRect(element);
					//					logger.log(jade.util.Logger.INFO, faceRectangle.toString());
					if (!faceRectangle.isNull() && faceRectangle.width()>0 && faceRectangle.height() > 0){
						
							person.setXinitialPoint(faceRectangle.x());
							person.setYinitialPoint(faceRectangle.y());
							person.setXfinalPoint(faceRectangle.x() + faceRectangle.width());
							person.setYfinalPoint(faceRectangle.y() + faceRectangle.height());
							person.setTimestamp(currentTime);
							if(firstDetectedPerson.getTimestamp() == 0){
								firstDetectedPerson = person;
							}
							else if ((faceRectangle.x() + faceRectangle.width()/2) < (firstDetectedPerson.getXmiddlePoint() + TRACK_RANGE)
									&& (faceRectangle.x() + faceRectangle.width()/2) > (firstDetectedPerson.getXmiddlePoint() - TRACK_RANGE)
									&& (faceRectangle.y() + faceRectangle.height()/2) < (firstDetectedPerson.getYmiddlePoint() + TRACK_RANGE)
									&& (faceRectangle.y() + faceRectangle.height()/2) > (firstDetectedPerson.getYmiddlePoint() - TRACK_RANGE)){
								firstDetectedPerson = person;
							}
							else if(secondDetectedPerson.getTimestamp() == 0){
								secondDetectedPerson = person;
							}
							else if ((faceRectangle.x() + faceRectangle.width()/2) < (secondDetectedPerson.getXmiddlePoint() + TRACK_RANGE)
									&& (faceRectangle.x() + faceRectangle.width()/2) > (secondDetectedPerson.getXmiddlePoint() - TRACK_RANGE)
									&& (faceRectangle.y() + faceRectangle.height()/2) < (secondDetectedPerson.getYmiddlePoint() + TRACK_RANGE)
									&& (faceRectangle.y() + faceRectangle.height()/2) > (secondDetectedPerson.getYmiddlePoint() - TRACK_RANGE)){
								secondDetectedPerson = person;
							}
							faceRectangle.deallocate();
							element.deallocate();
							person = null;
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		if(firstDetectedPerson.getTimestamp() != 0){
			detectedFaces.add(firstDetectedPerson);
		}
		if(secondDetectedPerson.getTimestamp() != 0){
			detectedFaces.add(secondDetectedPerson);
		}
		
		
		detectImage.release();
		cvReleaseImage(detectImage);
		facesContainer.deallocate();
		cvClearMemStorage(faceDetectorStorage);
		cvReleaseMemStorage(faceDetectorStorage);
		System.gc();
		return detectedFaces;
	}
}
