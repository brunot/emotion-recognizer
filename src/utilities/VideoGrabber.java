package utilities;

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.FrameRecorder;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.OpenCVFrameRecorder;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

public class VideoGrabber {
	public static void main(String args[]){
		CanvasFrame canvas = new CanvasFrame("Video");
		int i = 0;
		canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);   

		//Declare FrameGrabber to import video from "video.mp4"
		FrameGrabber grabber = new OpenCVFrameGrabber("s1_an_1.avi"); 
		try {      

			//Start grabber to capture video
			grabber.start();      

			//Declare img as IplImage
			IplImage img;

			while (i < grabber.getLengthInFrames()) {

				//inser grabed video fram to IplImage img
				img = grabber.grab();

				//Set canvas size as per dimentions of video frame.
				canvas.setCanvasSize(grabber.getImageWidth(), grabber.getImageHeight()); 

				if (img != null) {       
					//Show video frame in canvas
					canvas.showImage(img);               
				}
				
				i++;
			}
		}
		catch (Exception e) {      
		}
	}


	// Recorder -> might be useful when dealing with videos.
	// FrameRecorder rec = new OpenCVFrameRecorder("nada.avi",640,480);
	// rec.setFrameRate(14.985);
	//
	// try {
	// rec.start();
	// } catch (FrameRecorder.Exception ex) {
	// Logger.getLogger(javacv.class.getName()).log(Level.SEVERE, null, ex);
	// }

	// try {
	// rec.record(frame);
	// } catch (FrameRecorder.Exception ex) {
	// Logger.getLogger(javacv.class.getName()).log(Level.SEVERE, null, ex);
	// }

	// try {
	// rec.stop();
	// } catch (FrameRecorder.Exception ex) {
	// Logger.getLogger(javacv.class.getName()).log(Level.SEVERE, null, ex);
	// }
}
