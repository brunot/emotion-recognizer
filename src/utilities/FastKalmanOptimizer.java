//package utilities;
//
//import java.io.BufferedReader;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.io.UnsupportedEncodingException;
//import java.util.ArrayList;
//import java.util.concurrent.ExecutionException;
//import java.util.logging.LogManager;
//import com.googlecode.javacv.FrameGrabber;
//import com.googlecode.javacv.cpp.opencv_core.IplImage;
//
//import emotionrecognition.ContinuousEmotionRecognition;
//import emotionrecognition.InstantEmotionRecognition;
//import emotionrecognition.recognizedEmotion;
//import facedetection.DetectFaces;
//import facedetection.detectedFace;
//
//public class FastKalmanOptimizer {
//	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, InterruptedException, ExecutionException{
//		BufferedReader br;
//		String line;
//		new DetectFaces();
//		new InstantEmotionRecognition();
//		ContinuousEmotionRecognition continuousEmotionDetect = new ContinuousEmotionRecognition(0);
//		recognizedEmotion recognizedEmotion = null;
//		long currentTimeOfVideo = 0; //In millis
//		int videoLengthInFrames = 0;
//		int realEmotion = 0;
//		int globalEnergy = 0;
//		int globalEnergyLastStep = 1000000000;
//		double acceptanceProbability = 1000000000;
//
//		String csv_filename = "resources/IEmotions_25_2";
//
//		String[] lineArray = new String[2];
//		LogManager.getLogManager().reset();
//
//		// Simulated Annealing
//		double temperature = 50;
//		double roomTemperature = 10;
//		double K = 0.9995;
//		int iteration = 0;
//
//		// Optimized Parameters
//		double[] Q = new double[4];
//		double[] R = new double[4];
//		double bestEnergy = 0;
//
//		PrintWriter writerE = new PrintWriter("resources/kalmanE.txt", "UTF-8");
//		PrintWriter writerQ = new PrintWriter("resources/kalmanQ.txt", "UTF-8");
//		PrintWriter writerR = new PrintWriter("resources/kalmanR.txt", "UTF-8");
//		PrintWriter writerI = new PrintWriter("resources/kalmanI.txt", "UTF-8");
//		//		PrintWriter writerEmotion = new PrintWriter("resources/IEmotions.txt", "UTF-8");
//
//		continuousEmotionDetect.generateRandomQR();
//
//		System.out.println("Kalman optimization started");
//		
//		for(;temperature > roomTemperature; temperature = temperature*K){
//			try {
//				br = new BufferedReader(new FileReader(csv_filename));
//				while ((line = br.readLine()) != null) {
//					//					System.out.println("Step");
//					// use dot comma as separator
//					lineArray = line.split(",");
//					videoLengthInFrames = Integer.parseInt(lineArray[1]);
//					realEmotion = Integer.parseInt(lineArray[2]);
//					for(int j = 0; j < videoLengthInFrames; j++) {
//						line = br.readLine();
//						lineArray = line.split(",");
//						currentTimeOfVideo += 1000/25;
//						recognizedEmotion = new recognizedEmotion();						
//						recognizedEmotion.setNeutralDistance(Double.parseDouble(lineArray[0]));
//						recognizedEmotion.setAngerDistance(Double.parseDouble(lineArray[1]));
//						recognizedEmotion.setFearDistance(Double.parseDouble(lineArray[2]));
//						recognizedEmotion.setHappynessDistance(Double.parseDouble(lineArray[3]));
//						recognizedEmotion.setSadnessDistance(Double.parseDouble(lineArray[4]));
//						recognizedEmotion.setCreationTimestamp(currentTimeOfVideo);
//						continuousEmotionDetect.createParticleMovement(recognizedEmotion);
//						if (j > videoLengthInFrames/2){
////															globalEnergy++;
//							globalEnergy += continuousEmotionDetect.getParticleEmotion(realEmotion, 1);
//						}
//						System.gc();
//					} // END OF WHILE j
//					//						System.out.println(Runtime.getRuntime().freeMemory());
//					continuousEmotionDetect.clearParticle(0);
//					continuousEmotionDetect.clearEmotions();
//					continuousEmotionDetect.resetKalmanFilterParameters();
//
//				}
//
//
//
//
//				//				if( acceptanceProbability > Math.exp((globalEnergyLastStep-globalEnergy)/temperature)){
//				acceptanceProbability = Math.exp((globalEnergyLastStep-globalEnergy)/temperature);
//				//				}
//
//				if (Math.random()<acceptanceProbability){
//					globalEnergyLastStep = globalEnergy;
//					bestEnergy = globalEnergy;
//					System.out.println("Energy = " + bestEnergy);
//					Q = continuousEmotionDetect.getQ();
//					R = continuousEmotionDetect.getW();
//					System.out.println("Q = " + Q[0] + "," + Q[1] + "," + Q[2] + "," + Q[3]);
//					System.out.println("R = " + R[0] + "," + R[1] + "," + R[2] + "," + R[3]);
//					writerQ.println(Q[0] + "," + Q[1] + "," + Q[2] + "," + Q[3]);
//					writerR.println(R[0] + "," + R[1] + "," + R[2] + "," + R[3]);
//					writerE.println(bestEnergy);
//					writerI.println(iteration);
//				}
//
//				continuousEmotionDetect = new ContinuousEmotionRecognition(0);
//				recognizedEmotion = null;
//				new DetectFaces();
//				System.gc();
//				continuousEmotionDetect.generateRandomQR();
//				globalEnergy = 0;
//				iteration++;
//
//			} catch (FileNotFoundException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			} catch (NumberFormatException e) {
//
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			//			writerEmotion.close();
//			System.out.println("Next Iteration" + iteration);
//
//		}	
//		writerQ.close();
//		writerR.close();
//		writerE.close();
//		writerI.close();
//
//	}
//}
//
