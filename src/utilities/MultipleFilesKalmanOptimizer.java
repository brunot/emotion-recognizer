package utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import emotionrecognition.ContinuousEmotionRecognition;
import emotionrecognition.InstantEmotionRecognition;
import emotionrecognition.recognizedEmotion;
import facedetection.DetectFaces;
import facedetection.detectedFace;
import facerecognition.FaceRecognition;
import facerecognition.recognizedPerson;

public class MultipleFilesKalmanOptimizer {
	public static void main(String[] args) throws FileNotFoundException,
			UnsupportedEncodingException, InterruptedException,
			ExecutionException {
		BufferedReader br;
		String line;
		String videoName = null;
		FrameGrabber grabber;
		DetectFaces faceDetector = new DetectFaces();
		FaceRecognition recognizePerson = new FaceRecognition();
		InstantEmotionRecognition instantEmotionDetect = new InstantEmotionRecognition();
		ContinuousEmotionRecognition continuousEmotionDetect = new ContinuousEmotionRecognition(
				0);

		ArrayList<recognizedPerson> recognizedPerson = null;
		ArrayList<detectedFace> detected = null;
		ArrayList<recognizedEmotion> recognizedEmotion = null;
		IplImage frame;
		Logger logger = jade.util.Logger
				.getMyLogger("Kalman Parameters Optimizer");
		long currentTimeOfVideo = 0; // In millis
		int videoLengthInFrames = 0;
		int estimatedEmotion;
		int[] realEmotion = new int[362];
		int globalEnergy = 0;
		int globalEnergyLastStep = 1000000000;
		double acceptanceProbability = 1000000000;

		// String csv_filename = "resources/KalmanOptimizerVideos.csv";
		String csv_filename = "resources/AEmotion.csv";

		String[] lineArray = new String[2];
		LogManager.getLogManager().reset();

		// Simulated Annealing
		double temperature = 2500;
		double roomTemperature = 10.00;
		double K = 0.995;
		int iteration = 0;

		// Optimized Parameters
		double[] Q = new double[4];
		double[] R = new double[4];
		double bestEnergy = 0;

		PrintWriter writerE = new PrintWriter("resources/kalmanE.txt", "UTF-8");
		PrintWriter writerQ = new PrintWriter("resources/kalmanQ.txt", "UTF-8");
		PrintWriter writerR = new PrintWriter("resources/kalmanR.txt", "UTF-8");
		PrintWriter writerI = new PrintWriter("resources/kalmanI.txt", "UTF-8");
		PrintWriter writerK = new PrintWriter("resources/K.txt", "UTF-8");
		PrintWriter writerEmotion = new PrintWriter("resources/IEmotions.txt",
				"UTF-8");

		continuousEmotionDetect.generateRandomQR();

		System.out.println("Kalman optimization started");
		try {
			int i = 0;
			br = new BufferedReader(new FileReader(csv_filename));
			while ((line = br.readLine()) != null) {
				// System.out.println("Step");
				// use dot comma as separator

				lineArray = line.split(";");

				videoName = ("C:\\Users\\Bruno\\Videos\\Any Video Converter\\E\\A.mp4");
				realEmotion[i] = (Integer.parseInt(lineArray[1]));
				i++;
			}
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NumberFormatException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (; temperature > roomTemperature; temperature = temperature * K) {

			try {
				grabber = new OpenCVFrameGrabber(videoName);
				grabber.start();

				videoLengthInFrames = grabber.getLengthInFrames();
				int j = 0;

				while (j < videoLengthInFrames) {

					// insert grabed video fram to IplImage img
					frame = grabber.grab();
					j = grabber.getFrameNumber();
					currentTimeOfVideo += 1000 / 25;
					if (j % 1 == 0) {
						if (frame != null) {
							// System.out.println("Detection before:" +
							// System.currentTimeMillis());
							detected = faceDetector.detectFaces(frame,
									currentTimeOfVideo);
							recognizedPerson = recognizePerson.recognizeFaces(
									detected, frame);
							// System.out.println("Detection after:" +
							// System.currentTimeMillis());
							recognizedEmotion = instantEmotionDetect
									.RecognizeEmotionOnFaceList(
											recognizedPerson, frame);
							// System.out.println("Detection instant:" +
							// System.currentTimeMillis());

							// Save recognizedEmotions - Use if want to get a
							// file with emotions
							writerEmotion.println(recognizedEmotion.get(0)
									.getNeutralDistance()
									+ ","
									+ recognizedEmotion.get(0)
											.getAngerDistance()
									+ ","
									+ recognizedEmotion.get(0)
											.getFearDistance()
									+ ","
									+ recognizedEmotion.get(0)
											.getHappynessDistance()
									+ ","
									+ recognizedEmotion.get(0)
											.getSadnessDistance());

							continuousEmotionDetect.createParticlesMovement(
									recognizedEmotion);
							// System.out.println("Detection continuous:" +
							// System.currentTimeMillis());
						}

						if (j > videoLengthInFrames / 2 && j < 362) {
							// globalEnergy++;
							globalEnergy += continuousEmotionDetect
									.getParticleEmotion(realEmotion[j], 1, 0);
						}
						System.gc();
					}
				}
				writerEmotion.println("END");

				// END OF WHILE j
				grabber.stop();
				// System.out.println(Runtime.getRuntime().freeMemory());
				grabber.release();
				grabber = null;
				continuousEmotionDetect.clearParticle(0);
				continuousEmotionDetect.clearEmotions();
				continuousEmotionDetect.resetKalmanFilterParameters();

			} catch (Exception e) {
				e.printStackTrace();
			}

			// if( acceptanceProbability >
			// Math.exp((globalEnergyLastStep-globalEnergy)/temperature)){
			acceptanceProbability = Math
					.exp((globalEnergyLastStep - globalEnergy) / temperature);
			// }

			if (Math.random() < acceptanceProbability) {
				bestEnergy = globalEnergy;
				System.out.println("Energy = " + bestEnergy);
				Q = continuousEmotionDetect.getQ();
				R = continuousEmotionDetect.getW();
				System.out.println("Q = " + Q[0] + "," + Q[1] + "," + Q[2]
						+ "," + Q[3]);
				System.out.println("R = " + R[0] + "," + R[1] + "," + R[2]
						+ "," + R[3]);
				writerQ.println(Q[0] + "," + Q[1] + "," + Q[2] + "," + Q[3]);
				writerR.println(R[0] + "," + R[1] + "," + R[2] + "," + R[3]);
				writerE.println(bestEnergy);
				writerI.println(iteration);
				
			}

			continuousEmotionDetect = new ContinuousEmotionRecognition(0);
			detected = null;
			recognizedEmotion = null;
			frame = null;
			faceDetector = new DetectFaces();
			System.gc();
			continuousEmotionDetect.generateRandomQR();
			globalEnergyLastStep = globalEnergy;
			globalEnergy = 0;
			iteration++;
			writerEmotion.close();
			System.out.println("Next Iteration" + iteration);

		}

		writerQ.close();
		writerR.close();
		writerE.close();
		writerI.close();

	}
}
