package utilities;

import com.googlecode.javacpp.FloatPointer;
import com.googlecode.javacv.cpp.opencv_core.CvPoint;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import static com.googlecode.javacv.cpp.opencv_core.*;

public class DataUtilities {

	/**
	 * Converts the given float image to an unsigned character image.
	 * 
	 * @param srcImg
	 *            the given float image
	 * @return the unsigned character image
	 */
	public static IplImage convertFloatImageToUcharImage(IplImage srcImg) {
		IplImage dstImg;
		if ((srcImg != null) && (srcImg.width() > 0 && srcImg.height() > 0)) {
			// Spread the 32bit floating point pixels to fit within 8bit pixel
			// range.
			CvPoint minloc = new CvPoint();
			CvPoint maxloc = new CvPoint();
			double[] minVal = new double[1];
			double[] maxVal = new double[1];
			cvMinMaxLoc(srcImg, minVal, maxVal, minloc, maxloc, null);
			// Deal with NaN and extreme values, since the DFT seems to give
			// some NaN results.
			if (minVal[0] < -1e30) {
				minVal[0] = -1e30;
			}
			if (maxVal[0] > 1e30) {
				maxVal[0] = 1e30;
			}
			if (maxVal[0] - minVal[0] == 0.0f) {
				maxVal[0] = minVal[0] + 0.001; // remove potential divide by
												// zero errors.
			} // Convert the format
			dstImg = cvCreateImage(cvSize(srcImg.width(), srcImg.height()), 8,
					1);
			cvConvertScale(srcImg, dstImg, 255.0 / (maxVal[0] - minVal[0]),
					-minVal[0] * 255.0 / (maxVal[0] - minVal[0]));
			return dstImg;
		}
		return null;
	}


	/**
	 * Returns a string representation of the given float array.
	 * 
	 * @param floatArray
	 *            the given float array
	 * @return a string representation of the given float array
	 */
	public static String floatArrayToString(final float[] floatArray) {
		final StringBuilder stringBuilder = new StringBuilder();
		boolean isFirst = true;
		stringBuilder.append('[');
		for (int i = 0; i < floatArray.length; i++) {
			if (isFirst) {
				isFirst = false;
			} else {
				stringBuilder.append(", ");
			}
			stringBuilder.append(floatArray[i]);
		}
		stringBuilder.append(']');

		return stringBuilder.toString();
	}

	/**
	 * Returns a string representation of the given float pointer.
	 * 
	 * @param floatPointer
	 *            the given float pointer
	 * @return a string representation of the given float pointer
	 */
	public static String floatPointerToString(final FloatPointer floatPointer) {
		final StringBuilder stringBuilder = new StringBuilder();
		boolean isFirst = true;
		stringBuilder.append('[');
		for (int i = 0; i < floatPointer.capacity(); i++) {
			if (isFirst) {
				isFirst = false;
			} else {
				stringBuilder.append(", ");
			}
			stringBuilder.append(floatPointer.get(i));
		}
		stringBuilder.append(']');

		return stringBuilder.toString();
	}

	/**
	 * Returns a string representation of the given one-channel CvMat object.
	 * 
	 * @param cvMat
	 *            the given CvMat object
	 * @return a string representation of the given CvMat object
	 */
	public static String oneChannelCvMatToString(final CvMat cvMat) {
		// Preconditions
		if (cvMat.channels() != 1) {
			throw new RuntimeException(
					"illegal argument - CvMat must have one channel");
		}

		final int type = cvMat.type();
		StringBuilder s = new StringBuilder("[ ");
		for (int i = 0; i < cvMat.rows(); i++) {
			for (int j = 0; j < cvMat.cols(); j++) {
				if (type == CV_32FC1 || type == CV_32SC1) {
					s.append(cvMat.get(i, j));
				} else {
					throw new RuntimeException(
							"illegal argument - CvMat must have one channel and type of float or signed integer");
				}
				if (j < cvMat.cols() - 1) {
					s.append(", ");
				}
			}
			if (i < cvMat.rows() - 1) {
				s.append("\n  ");
			}
		}
		s.append(" ]");
		return s.toString();
	}
}