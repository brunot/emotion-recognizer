package utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.cpp.opencv_core.CvFileStorage;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import emotionrecognition.ContinuousEmotionRecognition;
import emotionrecognition.InstantEmotionRecognition;
import emotionrecognition.recognizedEmotion;
import facedetection.DetectFaces;
import facedetection.detectedFace;
import static com.googlecode.javacv.cpp.opencv_core.CV_STORAGE_WRITE;
import static com.googlecode.javacv.cpp.opencv_core.cvAttrList;
import static com.googlecode.javacv.cpp.opencv_core.cvOpenFileStorage;
import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
import static com.googlecode.javacv.cpp.opencv_core.cvWrite;
import static com.googlecode.javacv.cpp.opencv_core.cvWriteInt;
import static com.googlecode.javacv.cpp.opencv_highgui.cvSaveImage;

public class VideoFrameSaver {
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, InterruptedException, ExecutionException{
		BufferedReader br;
		String line;
		String videoName;
		FrameGrabber grabber;
		DetectFaces faceDetector = new DetectFaces();
		InstantEmotionRecognition instantEmotionDetect = new InstantEmotionRecognition();
		ContinuousEmotionRecognition continuousEmotionDetect = new ContinuousEmotionRecognition(0);
		ArrayList<detectedFace> detected = null;
		ArrayList<recognizedEmotion> recognizedEmotion = null;
		IplImage frame;
		long currentTimeOfVideo = 0; //In millis
		int videoLengthInFrames = 0;
		int estimatedEmotion;
		int realEmotion = 0;
		int globalEnergy = 0;
		int globalEnergyLastStep = 1000000000;
		double acceptanceProbability = 1000000000;

		String csv_filename = "resources/enterfacevideo.csv";

		String[] lineArray = new String[2];
		LogManager.getLogManager().reset();

		// Simulated Annealing
		double temperature = 2500;
		double roomTemperature = 10.00;
		double K = 0.995;
		int iteration = 0;

		// Optimized Parameters
		double[] Q = new double[4];
		double[] R = new double[4];
		double bestEnergy = 0;
		
		int emotion;

//		PrintWriter writer = new PrintWriter("resources/kalmanVerifier.txt");

//		continuousEmotionDetect.generateRandomQR();

		System.out.println("videoFrameGrabber");
		
		long debug;

		try {
			br = new BufferedReader(new FileReader(csv_filename));
			while ((line = br.readLine()) != null) {
				//					System.out.println("Step");
				// use dot comma as separator

				lineArray = line.split(";");

				videoName = (lineArray[0]);
				String name = videoName.split("\\\\")[8];
//				writer.println(videoName);
//				realEmotion = (Integer.parseInt(lineArray[1]));
				try{
					grabber = new OpenCVFrameGrabber(videoName); 
					grabber.start();

					
					
					videoLengthInFrames = grabber.getLengthInFrames();
					int j = 0;

					while (j < videoLengthInFrames) {

						//insert grabed video fram to IplImage img
						frame = grabber.grab();
						String filename = "output/enterface/" + name.split(".avi")[0] + "_" + j + ".jpg";
						System.out.println(filename);
						cvSaveImage(filename, frame);
//						cvSaveImage("output/videoFrames/a.jpg", frame);
						j = grabber.getFrameNumber();
						currentTimeOfVideo += 1000/25;
						
					}
//					writerEmotion.println("END");
					
					// END OF WHILE j
					grabber.stop();
//					System.out.println(Runtime.getRuntime().freeMemory());
					grabber.release();

				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
				

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NumberFormatException e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			writer.close();


	}
}

