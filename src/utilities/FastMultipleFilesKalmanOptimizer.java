package utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.LogManager;
import emotionrecognition.ContinuousEmotionRecognition;
import emotionrecognition.recognizedEmotion;

public class FastMultipleFilesKalmanOptimizer {
	public static void main(String[] args) throws FileNotFoundException,
			UnsupportedEncodingException, InterruptedException,
			ExecutionException {
		BufferedReader br_real;
		BufferedReader br_emotion;
		String line;

		ContinuousEmotionRecognition continuousEmotionDetect = new ContinuousEmotionRecognition(
				0);

		ArrayList<recognizedEmotion> recognizedEmotion = new ArrayList<recognizedEmotion>();

		long currentTimeOfVideo = 0; // In millis
		int videoLengthInFrames = 0;
		int[] realEmotion = new int[362];
		double[][] instantEmotions = new double[362][5];
		int globalEnergy = 0;
		int globalEnergyLastStep = 1000000000;
		double acceptanceProbability = 1000000000;

		// String csv_filename = "resources/KalmanOptimizerVideos.csv";
		String csv_filename = "resources/AEmotion.csv";
		String emotion_filename = "resources/IEmotions.txt";

		String[] lineArray = new String[2];
		LogManager.getLogManager().reset();

		// Simulated Annealing
		double temperature = 2500;
		double roomTemperature = 10.00;
		double K = 0.995;
		int iteration = 0;

		// Optimized Parameters
		double[] Q = new double[4];
		double[] R = new double[4];
		double bestEnergy = 0;

		PrintWriter writerE = new PrintWriter("resources/kalmanE.txt", "UTF-8");
		PrintWriter writerQ = new PrintWriter("resources/kalmanQ.txt", "UTF-8");
		PrintWriter writerR = new PrintWriter("resources/kalmanR.txt", "UTF-8");
		PrintWriter writerI = new PrintWriter("resources/kalmanI.txt", "UTF-8");
		PrintWriter writerK = new PrintWriter("resources/K.txt", "UTF-8");

		continuousEmotionDetect.generateRandomQR();

		System.out.println("Kalman optimization started");

		recognizedEmotion.add(new recognizedEmotion());

		try {
			int i = 0;
			br_real = new BufferedReader(new FileReader(csv_filename));
			br_emotion = new BufferedReader(new FileReader(emotion_filename));
			while ((line = br_real.readLine()) != null) {
				// System.out.println("Step");
				// use dot comma as separator

				lineArray = line.split(";");
				realEmotion[i] = (Integer.parseInt(lineArray[1]));
				i++;
			}
			i = 0;
			while ((line = br_emotion.readLine()) != null && i < 362) {
				// System.out.println("Step");
				// use dot comma as separator

				lineArray = line.split(",");
				instantEmotions[i][0] = (Double.parseDouble(lineArray[0]));
				instantEmotions[i][1] = (Double.parseDouble(lineArray[1]));
				instantEmotions[i][2] = (Double.parseDouble(lineArray[2]));
				instantEmotions[i][3] = (Double.parseDouble(lineArray[3]));
				instantEmotions[i][4] = (Double.parseDouble(lineArray[4]));
				i++;
			}

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NumberFormatException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (; temperature > roomTemperature; temperature = temperature * K) {

			int j = 0;

			while (j < 362) {

				// insert grabed video fram to IplImage img
				currentTimeOfVideo += 1000 / 25;
				if (j % 1 == 0 && j < 362) {
					recognizedEmotion.get(0).setNeutralDistance(
							(instantEmotions[j][0]));
					recognizedEmotion.get(0).setAngerDistance(
							(instantEmotions[j][1]));
					recognizedEmotion.get(0).setFearDistance(
							(instantEmotions[j][2]));
					recognizedEmotion.get(0).setHappynessDistance(
							(instantEmotions[j][3]));
					recognizedEmotion.get(0).setSadnessDistance(
							(instantEmotions[j][4]));
					recognizedEmotion.get(0).setCreationTimestamp(
							currentTimeOfVideo);
					continuousEmotionDetect
							.createParticlesMovement(recognizedEmotion);

					if (j < 362) {
						// globalEnergy++;
						globalEnergy += continuousEmotionDetect
								.getParticleEmotion(realEmotion[j], 1, 0);
					}
//					System.gc();
					j++;
				}
			}

			// END OF WHILE j
			continuousEmotionDetect.clearParticle(0);
			continuousEmotionDetect.clearEmotions();
			continuousEmotionDetect.resetKalmanFilterParameters();

			acceptanceProbability = Math
					.exp((globalEnergyLastStep - globalEnergy) / temperature);

			if (Math.random() < acceptanceProbability) {
				bestEnergy = globalEnergy;
				System.out.println("Energy = " + bestEnergy);
				Q = continuousEmotionDetect.getQ();
				R = continuousEmotionDetect.getW();
				System.out.println("Q = " + Q[0] + "," + Q[1] + "," + Q[2]
						+ "," + Q[3]);
				System.out.println("R = " + R[0] + "," + R[1] + "," + R[2]
						+ "," + R[3]);
				System.out.println(continuousEmotionDetect.k_conv);
				writerQ.println(Q[0] + "," + Q[1] + "," + Q[2] + "," + Q[3]);
				writerR.println(R[0] + "," + R[1] + "," + R[2] + "," + R[3]);
				writerE.println(bestEnergy);
				writerI.println(iteration);
				writerK.println(continuousEmotionDetect.k_conv);
			}

			continuousEmotionDetect = new ContinuousEmotionRecognition(0);
			System.gc();
			continuousEmotionDetect.generateRandomQR();
			globalEnergyLastStep = globalEnergy;
			globalEnergy = 0;
			iteration++;
			System.out.println("Next Iteration" + iteration);

		}

		writerQ.close();
		writerR.close();
		writerE.close();
		writerI.close();
		writerK.close();

	}
}
