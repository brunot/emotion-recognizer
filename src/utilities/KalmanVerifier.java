package utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.logging.LogManager;

import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.FrameGrabber.Exception;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import emotionrecognition.ContinuousEmotionRecognition;
import emotionrecognition.InstantEmotionRecognition;
import emotionrecognition.recognizedEmotion;
import facedetection.DetectFaces;
import facedetection.detectedFace;
import facerecognition.FaceRecognition;
import facerecognition.recognizedPerson;

public class KalmanVerifier {
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException, InterruptedException, ExecutionException{
		BufferedReader br;
		String line;
		String videoName;
		FrameGrabber grabber;
		DetectFaces faceDetector = new DetectFaces();
		FaceRecognition recognizePerson = new FaceRecognition();
		InstantEmotionRecognition instantEmotionDetect = new InstantEmotionRecognition();
		ContinuousEmotionRecognition continuousEmotionDetect = new ContinuousEmotionRecognition(0);
		ArrayList<detectedFace> detected = null;
		ArrayList<recognizedPerson> recognizedPerson = null;
		ArrayList<recognizedEmotion> recognizedEmotion = null;
		IplImage frame;
		long currentTimeOfVideo = 0; //In millis
		int videoLengthInFrames = 0;
		int realEmotion = 0;
		String csv_filename = "resources/A.csv";

		String[] lineArray = new String[2];
		LogManager.getLogManager().reset();

		int emotion;

		PrintWriter writer = new PrintWriter("resources/kalmanVerifier.txt");

//		continuousEmotionDetect.generateRandomQR();

		System.out.println("Kalman optimization started");
		
		try {
			br = new BufferedReader(new FileReader(csv_filename));
			while ((line = br.readLine()) != null) {
				//					System.out.println("Step");
				// use dot comma as separator

				lineArray = line.split(";");

				videoName = (lineArray[0]);
				writer.println(videoName);
				realEmotion = (Integer.parseInt(lineArray[1]));
				try{
					grabber = new OpenCVFrameGrabber(videoName); 
					grabber.start();

					
					
					videoLengthInFrames = grabber.getLengthInFrames();
					int j = 0;

					while (j < videoLengthInFrames) {

						//insert grabed video fram to IplImage img
						frame = grabber.grab();
						j = grabber.getFrameNumber();
						currentTimeOfVideo += 1000/25;
						if (j % 1 == 0){
							if (frame != null) {
//							System.out.println("Detection before:" + System.currentTimeMillis());
							detected = faceDetector.detectFaces(frame, currentTimeOfVideo);
							recognizedPerson = recognizePerson.recognizeFaces(detected, frame);
//							System.out.println("Detection after:" + System.currentTimeMillis());
							recognizedEmotion = instantEmotionDetect.RecognizeEmotionOnFaceList(recognizedPerson, frame);
//							System.out.println("Detection instant:" + System.currentTimeMillis());
							
							// Save recognizedEmotions - Use if want to et a file with emotions
//							writerEmotion.println(recognizedEmotion.get(0).getNeutralDistance() + "," + recognizedEmotion.get(0).getAngerDistance() + "," +
//									recognizedEmotion.get(0).getFearDistance() + "," + recognizedEmotion.get(0).getHappynessDistance() + "," + recognizedEmotion.get(0).getSadnessDistance());								
							
							continuousEmotionDetect.createParticlesMovement(recognizedEmotion);
//							System.out.println("Detection continuous:" + System.currentTimeMillis());
						}
							
						if (j > videoLengthInFrames/2){
//							globalEnergy++;
							emotion = continuousEmotionDetect.getParticleEmotion(0);
							writer.println(realEmotion + "," + emotion);
						}
						System.gc();
						}
					}
//					writerEmotion.println("END");
					
					// END OF WHILE j
					grabber.stop();
//					System.out.println(Runtime.getRuntime().freeMemory());
					grabber.release();
					grabber = null;
					continuousEmotionDetect.clearParticle(0);
					continuousEmotionDetect.clearEmotions();
					continuousEmotionDetect.resetKalmanFilterParameters();

				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
				

			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NumberFormatException e) {

				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			writer.close();


	}
}

