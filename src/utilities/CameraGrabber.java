package utilities;

import com.googlecode.javacv.FFmpegFrameGrabber;
import com.googlecode.javacv.FrameGrabber;
import com.googlecode.javacv.OpenCVFrameGrabber;
import com.googlecode.javacv.VideoInputFrameGrabber;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.FlyCaptureFrameGrabber;

import static com.googlecode.javacv.cpp.opencv_core.*;

public class CameraGrabber {
	public OpenCVFrameGrabber grabber;
	public IplImage frame;

	public CameraGrabber(){
		grabber = new OpenCVFrameGrabber("");
		frame = new IplImage();
	}
	
	/**
	 * Returns a frame from camera.
	 * 
	 * @return an IplImage of the frame
	 */
	public IplImage grabFrameFromCamera() {
//		grabber = new VideoInputFrameGrabber(0);
		try {
//			grabber = new VideoInputFrameGrabber(0);
			// Start grabber to capture video
			grabber.start();

			// insert grabbed video frame to IplImage img
			frame = grabber.grab();


			if (frame != null) {
				// Flip image horizontally
				cvFlip(frame, frame, 1);
			}
			
//			grabber.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return frame;
	}
}
