package utilities;

import static com.googlecode.javacv.cpp.opencv_core.CV_32FC1;
import static com.googlecode.javacv.cpp.opencv_core.CV_MINMAX;
import static com.googlecode.javacv.cpp.opencv_core.cvCloneImage;
import static com.googlecode.javacv.cpp.opencv_core.cvConvertScale;
import static com.googlecode.javacv.cpp.opencv_core.cvCopy;
import static com.googlecode.javacv.cpp.opencv_core.cvCreateImage;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSize;
import static com.googlecode.javacv.cpp.opencv_core.cvNormalize;
import static com.googlecode.javacv.cpp.opencv_core.cvResetImageROI;
import static com.googlecode.javacv.cpp.opencv_core.cvReshape;
import static com.googlecode.javacv.cpp.opencv_core.cvSetImageROI;
import static com.googlecode.javacv.cpp.opencv_core.cvSize;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvResize;
import static com.googlecode.javacv.cpp.opencv_core.*;

import com.googlecode.javacv.cpp.opencv_core.CvMat;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

public class ImageUtilities {
	private static final int FACE_RECT_SIZE = 200;
	
	public static IplImage cropAndResizeToFace(CvRect Rectangle, IplImage image_to_resize){
//		logger.log(jade.util.Logger.INFO,"Dimensions before crop: " + image_to_resize.width() + " x " + image_to_resize.height());
		IplImage tmp;
		IplImage resizedImage;
		
		cvSetImageROI(image_to_resize, Rectangle);
		tmp = cvCreateImage(cvGetSize(image_to_resize), image_to_resize.depth(), image_to_resize.nChannels());
		cvCopy(image_to_resize, tmp);
		cvResetImageROI(image_to_resize);
		resizedImage = cvCreateImage(cvSize(FACE_RECT_SIZE, FACE_RECT_SIZE), image_to_resize.depth(), image_to_resize.nChannels());
		cvResize(tmp, resizedImage);
		tmp.release();
		cvReleaseImage(tmp);
		
//		logger.log(jade.util.Logger.INFO,"Dimensions after crop: " + tmp.width() + " x " + tmp.height());		
		return resizedImage;
	}
	
    public static IplImage toReshapedAndNormalizedImage(CvMat vector){
        //Reshape
    	CvMat target = CvMat.create(200, 200, CV_32FC1);
        cvReshape(vector, target, 1, 200);
        
        //Normalize
        CvMat normalized = CvMat.create(target.rows(), target.cols(), target.type());
        cvNormalize(target, normalized, 0, 1, CV_MINMAX, null);
        
        //Scale
        IplImage image = IplImage.create(normalized.cols(), normalized.rows(), 8, normalized.channels());
        cvConvertScale(normalized, image, 255, 0);
        
//        target.release();
//        normalized.release();
        
        return image;
    }
}

