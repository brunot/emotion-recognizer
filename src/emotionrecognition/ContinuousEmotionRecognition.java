package emotionrecognition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class ContinuousEmotionRecognition {
	private static final int MAX_EMOTIONS_STORED = 50;
	private static ArrayList<String> names;
	private static List<ArrayList<recognizedEmotion>> recognizedInstantEmotions;
	private static List<ArrayList<particle>> particle;
	private final static double K = 5;
	private final static double Tal = 1.5;

	// Fixed covariances for Kalman
	private double[] w = {681.6238264636487,702.3832458005867,777.2833369491603,44.224270012152125};
	private double[] v = {85.42253177404757,303.01953745645517,539.1042036667512,701.5371476448564};

	// Kalman Parameters
	double[] xs = new double[4];
	double[] old_xs = new double[4];
	double[] p = new double[4];
	double[] y = new double[4];
	double[] m = new double[4];
	double[] r = new double[4];
	
	// Intensity of local minima convergence
	public double k_conv = 0.01;

	// Emotion offset parameters
	double[] offset_emotion = new double[4];

	// Ticker
	long timestamp = 0;
	
	// Writer Variables
	PrintWriter continuousEmotionWriter;
	PrintWriter instantEmotionWriter;
	PrintWriter emotionWriter;
	String separator = System.getProperty("line.separator");
	DecimalFormat decimalFormat;
	
	// Sink
	private static final double SINK_X_DIST = 0.5;
	private static final double SINK_Y_DIST = 0.5;
	private boolean sinkable_x = false;
	private boolean sinkable_y = false;
	
	private static final double EMOTION_RADIUS = 0;

	public ContinuousEmotionRecognition(long timestamp){
		recognizedInstantEmotions = new ArrayList<ArrayList<recognizedEmotion>>();
		names = new ArrayList<String>();
		particle = new ArrayList<ArrayList<particle>>();
		resetKalmanFilterParameters();
		initInstantWriter();
		initContinuousWriter();
		initEmotionWriter();
		decimalFormat = new DecimalFormat("00.000000");
		this.timestamp = timestamp;
	}
	
	public void initEmotionWriter(){
		// Recreate writer output text file
		try {
			emotionWriter = new PrintWriter("output/emotionOutput.txt", "UTF-8");
		} catch (FileNotFoundException e) {
			new File("/output").mkdirs();
			try {
				emotionWriter = new PrintWriter("output/emotionOutput.txt", "UTF-8");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		emotionWriter.println("Output de Emoções");
		emotionWriter.close();
	}
	
	public void initContinuousWriter() {
		// Recreate writer output text file
		try {
			continuousEmotionWriter = new PrintWriter("output/continuousEmotionRecognition.txt", "UTF-8");
		} catch (FileNotFoundException e) {
			new File("/output").mkdirs();
			try {
				continuousEmotionWriter = new PrintWriter("output/continuousEmotionRecognition.txt", "UTF-8");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		continuousEmotionWriter.println("Output de Emoção Contínua");
		continuousEmotionWriter.close();
	}
	
	public void initInstantWriter(){
		try {
			instantEmotionWriter = new PrintWriter("output/instantEmotionRecognition.txt", "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			new File("/output").mkdirs();
			try {
				instantEmotionWriter = new PrintWriter("output/instantEmotionRecognition.txt", "UTF-8");
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		instantEmotionWriter.println("Output de Emoções Instantâneas");
		instantEmotionWriter.close();
	}
	
	public void createParticlesMovement(ArrayList<recognizedEmotion> emotions){
		boolean has_emotion = false;
		timestamp = emotions.get(0).getCreationTimestamp();
		ArrayList<String> non_repeated_names = new ArrayList<String>();
		
		for(int i = 0; i < emotions.size(); i++){
			if (names.indexOf(emotions.get(i).getName()) == -1){
				names.add(emotions.get(i).getName());
				particle initialStateParticle = new particle();
				initialStateParticle.setCreationTimestamp(emotions.get(i).getCreationTimestamp());
				ArrayList<particle> particleArray = new ArrayList<particle>();
				ArrayList<recognizedEmotion> emotionArray = new ArrayList<recognizedEmotion>();
				particleArray.add(initialStateParticle);
				particle.add(particleArray);
				recognizedInstantEmotions.add(emotionArray);
				initialStateParticle = null;
			}
		}

		for(int i = 0; i < particle.size(); i++){
			for(int j = 0; j < emotions.size(); j++){
				if(names.indexOf(emotions.get(j).getName()) == i && non_repeated_names.indexOf(emotions.get(j).getName())==-1){
					createParticleMovement(emotions.get(j), i);
					has_emotion = true;
					non_repeated_names.add(emotions.get(j).getName());
					break;
				}
			}
			if (has_emotion == false){
				recognizedEmotion emotion = new recognizedEmotion();
				double[] distances = new double[5];
				for(int k = 0; k< distances.length; k++){
					distances[k] = 10000;
				}
				emotion.setAllDistances(distances);
				emotion.setCreationTimestamp(timestamp);
				createParticleMovement(emotion, i);
			}
			has_emotion = false;
		}
		
	}

	public void createParticleMovement(recognizedEmotion emotion, int index){
		addRecognizedEmotion(emotion, index);
		runKalmanFilter(index);
		appendToOutput();
	}

	private void appendToOutput() {
		// TODO Auto-generated method stub
		File outputContinuous = new File("output/continuousEmotionRecognition.txt");
		File outputEmotion = new File("output/emotionOutput.txt");
		
		try {
			continuousEmotionWriter = new PrintWriter(new FileWriter(outputContinuous, true));
			emotionWriter = new PrintWriter(new FileWriter(outputEmotion, true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		continuousEmotionWriter.print(timestamp + "\t");
		emotionWriter.print(timestamp + "\t");
		int j = 0;
		for(Iterator<ArrayList<emotionrecognition.particle>> i = particle.iterator(); i.hasNext(); ) {
		    ArrayList<emotionrecognition.particle> item = i.next();
		    double[] position = item.get(item.size()-1).getPosition();
		    continuousEmotionWriter.print(decimalFormat.format(position[0]) + "\t" + decimalFormat.format(position[1]));
		    int emotion = getParticleEmotion(j);
		    if (emotion == 1){
		    	emotionWriter.print("0\t1\t0\t0\t0\t");
		    } else if( emotion == 4){
		    	emotionWriter.print("0\t0\t1\t0\t0\t");
		    } else if (emotion == 5){
		    	emotionWriter.print("0\t0\t0\t1\t0\t");
		    } else if (emotion == 6){
		    	emotionWriter.print("0\t0\t0\t0\t1\t");
		    } else if (emotion == 0){
		    	emotionWriter.print("1\t0\t0\t0\t0\t");
		    }
		    j++;
		}
		
		continuousEmotionWriter.print(separator);
		emotionWriter.print(separator);
		
		continuousEmotionWriter.close();
		emotionWriter.close();
		
		File outputInstant = new File("output/instantEmotionRecognition.txt");
		try {
			instantEmotionWriter = new PrintWriter(new FileWriter(outputInstant, true));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		instantEmotionWriter.print(timestamp + "\t");
		Iterator<ArrayList<recognizedEmotion>> i = recognizedInstantEmotions.iterator();
		while(i.hasNext()) {
		    ArrayList<recognizedEmotion> item = i.next();
		    if(item.size()>0){
		    double[] emotion = item.get(item.size()-1).getAllEmotionsBase100();
		    double[] kalmanEmotion = item.get(item.size()-1).getKalmanPercentage();
		    instantEmotionWriter.print(decimalFormat.format(emotion[0]) + "\t" + decimalFormat.format(emotion[1]) + "\t" + decimalFormat.format(emotion[2]) + "\t" + decimalFormat.format(emotion[3]) + 
		    		"\t" + decimalFormat.format(kalmanEmotion[0]) + "\t" + decimalFormat.format(kalmanEmotion[1]) + "\t" + decimalFormat.format(kalmanEmotion[2]) + "\t" + decimalFormat.format(kalmanEmotion[3]) + "\t");
		    }}
		
		instantEmotionWriter.print(separator);
		instantEmotionWriter.close();
		
	}

	public List<ArrayList<recognizedEmotion>> getRecognizedInstantEmotions(){
		return recognizedInstantEmotions;
	}

	public List<ArrayList<particle>> getParticle(){
		return particle;
	}

	public void clearParticle(long timestamp){
		particle.clear();
		initializeMovement(timestamp);
	}

	public void clearEmotions(){
		recognizedInstantEmotions.clear();
	}

	@SuppressWarnings("unchecked")
	private void initializeMovement(long timestamp){
		particle = new ArrayList<ArrayList<particle>>();
	}

	private void addRecognizedEmotion(recognizedEmotion emotion, int index){
		ArrayList <recognizedEmotion> instantEmotion = recognizedInstantEmotions.get(index);
		
		
		
		if(instantEmotion.size() >= MAX_EMOTIONS_STORED){
			instantEmotion.remove(0);
		}
		instantEmotion.add(emotion);
	}

	public void SpecifyOffsetEmotions(recognizedEmotion emotion){
		offset_emotion = emotion.getAllEmotionsBase100();
	}

	private void runKalmanFilter(int index){
		recognizedEmotion emotions = recognizedInstantEmotions.get(index).get(recognizedInstantEmotions.get(index).size()-1);
		particle previousStepParticle = particle.get(index).get(particle.get(index).size()-1);
		r = offsetEmotion(emotions);
		long timeInterval = (Math.abs(previousStepParticle.getCreationTimestamp() - emotions.getCreationTimestamp()));
		int predictionIterations = 40;
		for(int j = 0; j < predictionIterations; j++){
			for(int i = 0; i < 4; i++){	

				// Predict
				xs[i] = old_xs[i];
//				p[i] = p[i] + w[i]/(Tal*Tal);
				p[i] = p[i] + w[i]*0.001*0.001;
				// Calculate output
				y[i] = K*xs[i]/Tal;
			}
//			emotions.setKalmanPercentage(y);
//			createNextStepParticle(1);
		}
		for(int i = 0; i < 4; i++){	
			// Update
			m[i] = p[i]*(K/Tal)/(p[i]*(K/Tal)*(K/Tal)+v[i]);
			xs[i] = xs[i] + m[i]*(r[i]-y[i]);
			p[i] = (1-m[i]*K/Tal)*p[i];

			// Calculate output
			y[i] = K/(Tal)*xs[i];

			// Store old_xs for next iteration
			old_xs[i] = xs[i];

		}
		// Set the correspondent kalman filtered distance
		emotions.setKalmanPercentage(y);
		createNextStepParticle(timeInterval, index);
	}

	public void resetKalmanFilterParameters(){
		// In Java everything not explicitly set is initialized to zero value
		xs = new double[4];
		old_xs = new double[4];
		p = new double[4];
		y = new double[4];
		m = new double[4];
		r = new double[4];		
	}

	private double[] offsetEmotion(recognizedEmotion emotions){
		double[] offsetEmotionsPercentages  = new double[4];
		double[] emotionsPercentages = emotions.getAllEmotionsBase100();

		for(int i = 0; i<offsetEmotionsPercentages.length; i++){
			offsetEmotionsPercentages[i] = emotionsPercentages[i] - offset_emotion[i];
			if( offsetEmotionsPercentages[i] < 0){
				offsetEmotionsPercentages[i] = 0.0;
			} else if ( offsetEmotionsPercentages[i] > 1){
				offsetEmotionsPercentages[i] = 1.0;
			}
		}

		return offsetEmotionsPercentages;
	}
	
	private double[] offsetKalmanEmotion(recognizedEmotion emotions){
		double[] emotionsPercentages  = new double[4];
		double[] kalmanPercentages = emotions.getKalmanPercentage();

		for(int i = 0; i<emotionsPercentages.length; i++){
			emotionsPercentages[i] = kalmanPercentages[i] - offset_emotion[i];
			if( emotionsPercentages[i] < 0){
				emotionsPercentages[i] = 0.0;
			} else if ( emotionsPercentages[i] > 1){
				emotionsPercentages[i] = 1.0;
			}
		}

		emotions.setKalmanPercentage(emotionsPercentages);

		return emotionsPercentages;
	}

	private void createNextStepParticle(long interval, int index){
		particle nextStepParticle = new particle();
		particle previousStepParticle = particle.get(index).get(particle.get(index).size()-1);
		recognizedEmotion emotions = recognizedInstantEmotions.get(index).get(recognizedInstantEmotions.get(index).size()-1);
		double[] nextParticleSlidingVelocity = new double[2];
		double[] nextParticlePosition = new double[3];

		if(particle.get(index).size() >= MAX_EMOTIONS_STORED){
			particle.get(index).remove(0);
		}

		double timeInterval = interval*1.0/1000;

		previousStepParticle.updateTotalParticleAttractorVelocity(emotions.getKalmanPercentage());
		for(int i = 0; i < 2; i++){
			nextParticleSlidingVelocity[i] = previousStepParticle.getTotalAttractorVelocity()[i] + previousStepParticle.getTotalSlidingVelocity()[i] - 2*k_conv*previousStepParticle.getPosition()[i];
			if (interval != 0)
				nextParticlePosition[i] = previousStepParticle.getPosition()[i] + (nextParticleSlidingVelocity[i])*timeInterval;
			else
				nextParticlePosition[i] = previousStepParticle.getPosition()[i];
		}
		nextParticlePosition[2] = nextStepParticle.getA()*nextParticlePosition[0]*nextParticlePosition[0] +  nextStepParticle.getB()*nextParticlePosition[1]*nextParticlePosition[1];
		
		if(Math.abs(nextParticlePosition[0]) > SINK_X_DIST && !sinkable_x){
			sinkable_x = true;
		}
		else if(sinkable_x){
			if(Math.abs(nextParticlePosition[0]) < SINK_X_DIST){
				sinkable_x = false;
				nextParticlePosition[0] = 0;
				nextParticleSlidingVelocity[0] = 0;
			}
		}
		
		if(Math.abs(nextParticlePosition[1]) > SINK_Y_DIST && !sinkable_y){
			sinkable_y = true;
		}
		else if(sinkable_y){
			if(Math.abs(nextParticlePosition[1]) < SINK_Y_DIST){
				sinkable_y = false;
				nextParticlePosition[1] = 0;
				nextParticleSlidingVelocity[1] = 0;
			}
		}
		
		nextStepParticle.setTotalSlidingVelocity(nextParticleSlidingVelocity);
		nextStepParticle.setPosition(nextParticlePosition);
		nextStepParticle.setCreationTimestamp(previousStepParticle.getCreationTimestamp() + interval);

		particle.get(index).add(nextStepParticle);
		previousStepParticle = null;
		nextStepParticle = null;
	}

	private long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}

	public int getParticleEmotion(int InputEmotion, int particleNumber, int index){
		int emotion = 0;
		int count = 0;
		double[] position = null;
		for (int i = 0; i<particleNumber; i++){
			position = particle.get(index).get(particle.get(index).size()-1-i).getPosition();
			if( position[0] > EMOTION_RADIUS ){
				if (position[1] > EMOTION_RADIUS)
					emotion = 5;
				else if (position[1] < -EMOTION_RADIUS)
					emotion = 4;
			}
			else if (position[0] < -EMOTION_RADIUS){
				if (position[1] > EMOTION_RADIUS)
					emotion = 1;
				else if (position[1] < -EMOTION_RADIUS)
					emotion = 6;
			}
			if (InputEmotion!= emotion)
				count++;
		}
//		System.out.println(position[0] + "   "+ position[1]);
		return count;
	}

	public void generateRandomQR(){
		for( int i = 0; i < 4; i++){
			w[i] = Math.random()*1000;
			v[i] = Math.random()*1000;
			k_conv = Math.random()/100;
		}
	}

	public double[] getQ(){
		return w;
	}

	public double[] getW(){
		return v;
	}

	public int getParticleEmotion(int index) {
		int emotion = 0;
		double[] position;
		position = particle.get(index).get(particle.get(index).size()-1).getPosition();
		if( position[0] >  EMOTION_RADIUS){
			if (position[1] > EMOTION_RADIUS)
				emotion = 5;
			else if (position[1] < -EMOTION_RADIUS)
				emotion = 4;
		}
		else if (position[0] < -EMOTION_RADIUS){
			if (position[1] > EMOTION_RADIUS)
				emotion = 1;
			else if (position[1] < -EMOTION_RADIUS)
				emotion = 6;
		}
		return emotion;
	}

}
