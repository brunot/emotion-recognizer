package emotionrecognition;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;
import java.util.logging.Logger;

import utilities.ImageUtilities;

import com.googlecode.javacv.cpp.opencv_contrib.FaceRecognizer;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import facedetection.detectedFace;
import facerecognition.recognizedPerson;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_RGB2GRAY;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;
import static com.googlecode.javacv.cpp.opencv_contrib.*;


public class InstantEmotionRecognition {
	private static FaceRecognizer emotionClassifier;

	// Emotion means
	static CvMat mean_neutral = CvMat.create(1, 4);
	static CvMat mean_happyness = CvMat.create(1, 4);
	static CvMat mean_anger = CvMat.create(1, 4);
	static CvMat mean_sadness = CvMat.create(1, 4);
	static CvMat mean_fear = CvMat.create(1, 4);
	
	// Emotion minimal distances
	static CvMat minimal_distance_neutral = CvMat.create(1, 4);
	static CvMat minimal_distance_happyness = CvMat.create(1, 4);
	static CvMat minimal_distance_anger = CvMat.create(1, 4);
	static CvMat minimal_distance_sadness = CvMat.create(1, 4);
	static CvMat minimal_distance_fear = CvMat.create(1, 4);
	
	// Emotion recognition Parameters
	static CvMat mean;
	static CvMat eigenvectors;
	static CvMat eigenvalues;
	
    static Logger logger = jade.util.Logger.getMyLogger("InstantEmotionRecognition");
    static InstantEmotionRecognitionIO emotionRecognitionIO = new InstantEmotionRecognitionIO();

	public enum EmotionToNumber {
	    NEUTRAL(0), ANGER(1), FEAR(4), HAPPY(5), SADNESS(6);

	    private int emotionToNumber;

	    private static Map<Integer, EmotionToNumber> map = new HashMap<Integer, EmotionToNumber>();

	    static {
	        for (EmotionToNumber legEnum : EmotionToNumber.values()) {
	            map.put(legEnum.emotionToNumber, legEnum);
	        }
	    }

	    private EmotionToNumber(final int leg) { emotionToNumber = leg; }

	    public static EmotionToNumber valueOf(int EmotionToNumber) {
	        return map.get(EmotionToNumber);
	    }
	}
	

	public InstantEmotionRecognition(){
		initFisherEmotionClassifier();
		initFisherResults();
		
	}
	
	
	
	public static void initFisherEmotionClassifier(){
		emotionClassifier = createFisherFaceRecognizer();
		emotionRecognitionIO.load(emotionClassifier);
		logger.log(jade.util.Logger.INFO,"Training finished");
	}

	private static void initFisherResults(){
		initMeanProjections();
		mean = emotionClassifier.getMat("mean");
		eigenvectors = emotionClassifier.getMat("eigenvectors");
		eigenvalues = emotionClassifier.getMat("eigenvalues");
	}
	
	public ArrayList<recognizedEmotion> RecognizeEmotionOnFaceList(ArrayList<recognizedPerson> recognizedPerson, IplImage image_to_recognize){
		int total = recognizedPerson.size();
		IplImage cropped_image = null;
		IplImage face;
        ArrayList<recognizedEmotion> recognizedEmotion = new ArrayList<recognizedEmotion>();
        CvRect r;
        detectedFace detected;
        recognizedEmotion person;
        for (int i = 0; i < total; i++) {
            
            // copia para nova matriz e salva em arquivo
        	detected = recognizedPerson.get(i).getDetectedFace();
            r = new CvRect(detected.getXinitialPoint(), detected.getYinitialPoint(),
            		detected.getXfinalPoint()-detected.getXinitialPoint(),
            		detected.getYfinalPoint()-detected.getYinitialPoint());
            cropped_image = ImageUtilities.cropAndResizeToFace(r, image_to_recognize);

            face = cvCreateImage(cvGetSize(cropped_image),
                    IPL_DEPTH_8U,
                    1);
            cvCvtColor(cropped_image, face, CV_RGB2GRAY);
            
//            IplImage faceR = ImageUtilities.cropAndResizeToFace(r, face);
//            cvResize(face, faceR, CV_INTER_LINEAR);
//            cvResetImageROI(image_to_recognize);
            cvSaveImage("Teste.jpg", face);
//            CvMat equalized_image = face.asCvMat();
//            
//            for(int j=0; j<equalized_image.rows(); j++){
//            	for(int k=0; k<equalized_image.cols(); k++){
//            		if ( 3.54*equalized_image.get(j, k) > 255){
//            			equalized_image.put(j, k, 255);
//            		}
//            		else{
//            			equalized_image.put(j, k, 3.54*equalized_image.get(j, k));
//            		}
//            	}
//            }
            

            // Faz o reconhecimendo e imprime o nome da emo��o
            person = emotionRecognizer(face);

            /* Check this for extraction */
            // Ponto da impressao do nome da emoção
            person.setXpontoEmotion(r.x());
            person.setYpontoEmotion(r.y() - 20); 
            person.setCreationTimestamp(detected.getTimestamp());
            person.setName(recognizedPerson.get(i).getName());
            recognizedEmotion.add(person);
            
            face.release();
            cropped_image.release();
            cvReleaseImage(cropped_image);
            cvReleaseImage(face);
            r.deallocate();
            person = null;
            detected = null;
            System.gc();
        }
        return recognizedEmotion;
	}
	
	public static recognizedEmotion emotionRecognizer(IplImage image_to_recognize){
		double[] distance = new double[5];
		recognizedEmotion emotion = new recognizedEmotion();
		CvMat image = null;
		CvMat float_image = null;
		CvMat oneRow = null;
		CvMat subspaceProject = null;
		
		IplImage cloned_image = cvCloneImage(image_to_recognize);
		
		image = cloned_image.asCvMat();
		float_image = CvMat.create(image.cols(), image.rows(), CV_64FC1);
		cvConvert(image, float_image);
		oneRow = CvMat.create(1, image.cols()*image.rows(), CV_64FC1);
		
		for(int i = 0; i < image.rows(); i++){
			for(int j = 0; j < image.cols(); j ++){
			oneRow.put(0,i*image.cols()+j, float_image.get(i, j));
			}
		}
		
		

		subspaceProject = subspaceProject(eigenvectors, mean, oneRow);
		
		logger.log(jade.util.Logger.INFO, subspaceProject.toString());	
		
		distance[0] = cvNorm(subspaceProject, mean_neutral, NORM_L2);
		distance[1] = cvNorm(subspaceProject, mean_anger, NORM_L2);
		distance[2] = cvNorm(subspaceProject, mean_fear, NORM_L2);
		distance[3] = cvNorm(subspaceProject, mean_happyness, NORM_L2);
		distance[4] = cvNorm(subspaceProject, mean_sadness, NORM_L2);
		
		minimal_distance_neutral.release();
		minimal_distance_anger.release();
		minimal_distance_fear.release();
		minimal_distance_happyness.release();
		minimal_distance_sadness.release();
		
		emotion.setAllDistances(distance);
//		emotion.setMinimalDistance(min_distance);
		
		cvReleaseImage(cloned_image);
		cloned_image.release();
		image.release();
		float_image.release();
		oneRow.release();
		
		subspaceProject.release();
		
		image = null;
		float_image = null;
		oneRow = null;
		subspaceProject = null;
		distance = null;
		
		return emotion;
	}
	
	private static double normalizedEuclidean(CvMat first_input, CvMat second_input){
		double distance = 0.0;
		double mean_first = 0.0;
		double mean_second = 0.0;
		for(int i = 0; i < first_input.length(); i++){
			mean_first += first_input.get(0, i);
			mean_second += second_input.get(0, i);
		}
		mean_first = mean_first/first_input.length();
		mean_second = mean_second/second_input.length();
		
		distance = ((first_input.get(0, 0)-mean_first-second_input.get(0, 0)+mean_second)* (first_input.get(0, 0)-mean_first-second_input.get(0, 0)+mean_second) +
				(first_input.get(0, 1)-mean_first-second_input.get(0, 1)+mean_second)*(first_input.get(0, 1)-mean_first-second_input.get(0, 1)+mean_second)+
				(first_input.get(0, 2)-mean_first-second_input.get(0, 2)+mean_second)*(first_input.get(0, 2)-mean_first-second_input.get(0, 2)+mean_second) +
				(first_input.get(0, 3)-mean_first-second_input.get(0, 3)+mean_second)* (first_input.get(0, 3)-mean_first-second_input.get(0, 3)+mean_second))/
				(2*((first_input.get(0, 0)-mean_first)*(first_input.get(0, 0)-mean_first)+(first_input.get(0, 1)-mean_first)*(first_input.get(0, 1)-mean_first)
						+(first_input.get(0, 2)-mean_first)*(first_input.get(0, 2)-mean_first)+(first_input.get(0, 3)-mean_first)*(first_input.get(0, 3)-mean_first)
						+(second_input.get(0, 0)-mean_first)*(second_input.get(0, 0)-mean_first)+(second_input.get(0, 1)-mean_first)*(second_input.get(0, 1)-mean_first)
						+(second_input.get(0, 2)-mean_first)*(second_input.get(0, 2)-mean_first)+(second_input.get(0, 3)-mean_first)*(second_input.get(0, 3)-mean_first)));
			
		return distance;
	}
	
	private static double LDASoft(CvMat first_input, CvMat second_input){
		double distance = 0.0;
		for(int i = 0; i < first_input.length(); i++){
			distance += Math.pow(eigenvalues.get(0, i), 0.2)*(first_input.get(0, i)- second_input.get(0, i))*(first_input.get(0, i)- second_input.get(0, i));
		}
		return distance;
	}
	
	private static double MahalinobisCosine(CvMat first_input, CvMat second_input){
		double distance = 0.0;
		double scalar = 0.0;
		double m;
		double n;
		double norm_m = 0.0;
		double norm_n = 0.0;
		for( int i = 0; i < first_input.length(); i++){
			m = first_input.get(0, i)/eigenvalues.get(0, i);
			n = second_input.get(0, i)/eigenvalues.get(0, i);
			scalar += m*n;
			norm_m += m*m;
			norm_n += n*n;
		}
		distance = Math.abs(scalar/(Math.sqrt(norm_m)*Math.sqrt(norm_n)));
		
		return distance;
	}

	private static void initMeanProjections(){
		MatVector projections = emotionClassifier.getMatVector("projections");
		CvMat labels = emotionClassifier.getMat("labels");
		cvZero(mean_neutral);
		cvZero(mean_happyness);
		cvZero(mean_anger);
		cvZero(mean_sadness);
		cvZero(mean_fear);
		int number_of_samples_per_emotion = (int) (projections.size()/5);
		
		for(int i = 0; i<projections.size(); i++){
			CvMat projection = projections.getCvMat(i);
			if(labels.get(i) == 0){
				mean_neutral = addToMean(mean_neutral, projection, number_of_samples_per_emotion);
			}
			else if(labels.get(i) == 1){
				mean_anger = addToMean(mean_anger, projection, number_of_samples_per_emotion);
			}
			else if(labels.get(i) == 4){
				mean_fear = addToMean(mean_fear, projection, number_of_samples_per_emotion);
			}
			else if(labels.get(i) == 5){
				mean_happyness = addToMean(mean_happyness, projection, number_of_samples_per_emotion);
			}
			else if(labels.get(i) == 6){
				mean_sadness = addToMean(mean_sadness, projection, number_of_samples_per_emotion);
			}
		}
		
		projections.deallocate();
		labels.release();
	}
	
	private static CvMat addToMean(CvMat mean, CvMat projection, int number_of_samples_per_emotion){
		for(int j=0; j<projection.cols(); j++){
			mean.put(0, j, projection.get(0, j)/number_of_samples_per_emotion + mean.get(0, j));
		}	
		return mean;
	}


	// It is necessary that the vector are distributed in a specified manner
	private static void calculateMinimalDistances(CvMat input_vector){
		MatVector projections = emotionClassifier.getMatVector("projections");
		CvMat labels = emotionClassifier.getMat("labels");
		cvZero(minimal_distance_neutral);
		cvZero(minimal_distance_happyness);
		cvZero(minimal_distance_anger);
		cvZero(minimal_distance_sadness);
		cvZero(minimal_distance_fear);
		double minimal_neutral_distance_value = 10000000;
		double minimal_happyness_distance_value = 10000000;
		double minimal_anger_distance_value = 10000000;
		double minimal_sadness_distance_value = 10000000;
		double minimal_fear_distance_value = 10000000;
		

		int number_of_people_projected = (int) projections.size();
		
		
		for(int i = 0; i<number_of_people_projected; i++){
			CvMat projection = projections.getCvMat(i);
			if(labels.get(i) == 0){
				if( minimal_neutral_distance_value > cvNorm(projection, input_vector, NORM_L2)){
					minimal_neutral_distance_value = cvNorm(projection, input_vector, NORM_L2);
					minimal_distance_neutral.release();
					minimal_distance_neutral = projection.clone();
				}
			}
			else if(labels.get(i) == 1){
				if( minimal_anger_distance_value > cvNorm(projection, input_vector, NORM_L2)){
					minimal_anger_distance_value = cvNorm(projection, input_vector, NORM_L2);
					minimal_distance_anger.release();
					minimal_distance_anger = projection.clone();
				}
			}
			else if(labels.get(i) == 4){
				if( minimal_fear_distance_value > cvNorm(projection, input_vector, NORM_L2)){
					minimal_fear_distance_value = cvNorm(projection, input_vector, NORM_L2);
					minimal_distance_fear.release();
					minimal_distance_fear = projection.clone();
				}
			}
			else if(labels.get(i) == 5){
				if( minimal_happyness_distance_value > cvNorm(projection, input_vector, NORM_L2)){
					minimal_happyness_distance_value = cvNorm(projection, input_vector, NORM_L2);
					minimal_distance_happyness.release();
					minimal_distance_happyness = projection.clone();
				}
			}
			else if(labels.get(i) == 6){
				if( minimal_sadness_distance_value > cvNorm(projection, input_vector, NORM_L2)){
					minimal_sadness_distance_value = cvNorm(projection, input_vector, NORM_L2);
					minimal_distance_sadness.release();
					minimal_distance_sadness = projection.clone();
				}
			}
			
			projection.release();
		}
		
		projections.deallocate();
		labels.release();
	}
	
	private static double getMinimalDistanceAmongClasses(){
		double minimal_distance;
		MatVector minimal_distances = new MatVector(5);
		minimal_distances.put(0, minimal_distance_neutral);
		minimal_distances.put(1, minimal_distance_anger);
		minimal_distances.put(2, minimal_distance_fear);
		minimal_distances.put(3, minimal_distance_happyness);
		minimal_distances.put(4, minimal_distance_sadness);
		
		minimal_distance = cvNorm(minimal_distances.getCvMat(1), minimal_distances.getCvMat(2), NORM_L2);
		
		for(int i = 0; i<minimal_distances.size(); i++){
			for(int j = 0; j<minimal_distances.size(); j++){
				if (i != j){
					double distance = cvNorm(minimal_distances.getCvMat(i), minimal_distances.getCvMat(j), NORM_L2);
					if (distance > minimal_distance){
						minimal_distance = distance;
					}
				}
			}
		}
		
		logger.log(jade.util.Logger.INFO, "Minimal Distance = " + minimal_distance);
		minimal_distances.deallocate();
		return minimal_distance;
	}
}
