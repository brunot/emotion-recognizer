package emotionrecognition;

import java.util.Date;

public class recognizedEmotion implements java.io.Serializable {
    
    private double[] distance = new double[5];
    private double[] kalmanPercentage = new double[4];
    private String name;
    private int XpontoEmotion;
    private int YpontoEmotion;
    private double sumDistances;
    private double minimal_distance;
    private long creation_timestamp;

    public int getXpontoEmotion() {
        return XpontoEmotion;
    }

    public void setXpontoEmotion(int XpontoEmotion) {
        this.XpontoEmotion = XpontoEmotion;
    }

    public int getYpontoEmotion() {
        return YpontoEmotion;
    }

    public void setYpontoEmotion(int YpontoEmotion) {
        this.YpontoEmotion = YpontoEmotion;
    }
    
    public double getNeutralDistance(){
    	return distance[0];
    }
  
    public double getAngerDistance(){
    	return distance[1];
    }
    
    public double getFearDistance(){
    	return distance[2];
    }
    
    public double getHappynessDistance(){
    	return distance[3];
    }
    
    public double getSadnessDistance(){
    	return distance[4];
    }

    public double[] getAllDistanceBase100(){
        return transformDistanceToBase100();
    }
    
    public double[] getAllEmotionsBase100(){
        double[] transformedDistance = new double[4];
        double sumTransformedDistance = 0.0;
        double aux;
//        // Assuming Mahalinobis cosine
//        for(int i = 0; i<transformedDistance.length; i++){
//        	transformedDistance[i] = Math.abs(((1-distance[i+1])));
//        }
        
//         Distance normalized by mean
        
        // First iteration to sum neutral distance
        sumTransformedDistance = Math.abs(((distance[0]-meanDistance())));
//        for(int i = 0; i<transformedDistance.length; i++){
//        	transformedDistance[i] = Math.abs(((distance[i+1]-meanDistance())));
//        	sumTransformedDistance += transformedDistance[i];
//        }
//        for(int i = 0; i<transformedDistance.length; i++){
//        	if(distance[i+1]-meanDistance() > 0 )
//        		for(int j = i+1; j<transformedDistance.length; j++){
//        			if(distance[i+1]<distance[j+1]){
////        				System.out.println("mean=" + meanDistance());
////        				System.out.println(distance[i+1] + " " + i+1);
////        				System.out.println(distance[j+1] + " " + j+1);
//        				aux = transformedDistance[j];
//        				transformedDistance[j] = transformedDistance[i];
//        				transformedDistance[i] = aux;
//        			}
//        		}
//        }
//        for(int i = 0; i<transformedDistance.length; i++){
//        	transformedDistance[i] = transformedDistance[i]/sumTransformedDistance;
//        }
        
        for(int i = 0; i< transformedDistance.length; i++){
        	transformedDistance[i] = 1/Math.exp((distance[i+1])/300);
        }
		return transformedDistance;  	
    }
    
    private double[] transformDistanceToBase100(){
        double[] transformedDistance = new double[5];
        double sumTransformedDistance = 0.0;
        
        
//        
//        // Assuming Mahalinobis cosine
//        for(int i = 0; i<transformedDistance.length; i++){
//        	transformedDistance[i] = Math.abs(((1-distance[i])));
//        }
        
//        for(int i = 0; i<distance.length; i++){
//        	if(distance[i]>max)
//        		max = distance[i];
//        	if(distance[i]<min)
//        		min = distance[i];
//        }
        
        for(int i = 0; i<transformedDistance.length; i++){
        	transformedDistance[i] = 1/distance[i];
//            transformedDistance[i] = Math.abs((distance[i]-meanDistance()));
//        	transformedDistance[i] = Math.abs((max-distance[i])/(max-min));
            	sumTransformedDistance += transformedDistance[i];
        }
        
//        double upperSum = 0;
//        double lowerSum = 0;
//        for(int i = 0; i < transformedDistance.length; i++){
//        	if(distance[i] > meanDistance())
//        		lowerSum += distance[i]-meanDistance();
//        	else
//        		upperSum += meanDistance()-distance[i];
//        }
//        
//        for(int i = 0; i < transformedDistance.length; i++){
//        	if(distance[i] > meanDistance())
//        		transformedDistance[i] = upperSum/(distance[i]-meanDistance());
//        	else
//        		transformedDistance[i] = (meanDistance()-distance[i])/lowerSum;
//        	sumTransformedDistance += transformedDistance[i];
//        }
        
        for(int i = 0; i<transformedDistance.length; i++){
        		transformedDistance[i] = transformedDistance[i]/sumTransformedDistance;
//        	secondSum += transformedDistance[i];
        }
           
		return transformedDistance;
    }

    public void setNeutralDistance(double distance){
    	this.distance[0] = distance;
        sumDistances();
    }
  
    public void setAngerDistance(double distance){
    	this.distance[1] = distance;
        sumDistances();
    }
    
    public void setFearDistance(double distance){
    	this.distance[2] = distance;
        sumDistances();
    }
    
    public void setHappynessDistance(double distance){
    	this.distance[3] = distance;
        sumDistances();
    }
    
    public void setSadnessDistance(double distance){
    	this.distance[4] = distance;
        sumDistances();
    }

    public void setAllDistances(double[] set_distance){
        for(int i=0; i<distance.length; i++){
        	distance[i] = set_distance[i];
        }
        sumDistances();
    }

    public void setAllEmotionsDistances(double[] set_distance){
        for(int i=1; i<distance.length; i++){
        	distance[i] = set_distance[i-1];
        }
        sumDistances();
    }
    
    public double[] getAllEmotionDistances(){
    	double[] emotionDistances= new double[4];
        for(int i=1; i<distance.length; i++){
        	emotionDistances[i-1] = distance[i];
        }
    	return emotionDistances;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    
	public double getMinimalDistance() {
		return minimal_distance;
	}

	public void setMinimalDistance(double minimal_distance) {
		this.minimal_distance = minimal_distance;
	}

	public long getCreationTimestamp() {
		return creation_timestamp;
	}

	public void setCreationTimestamp(long timestamp) {
		this.creation_timestamp = timestamp;
	}
	
	// Si-m=di
	// di/sum(di)

    private void sumDistances(){
        sumDistances = 0.0;
        for(int i =0; i<distance.length; i++){
            sumDistances += distance[i];
        }        
    }
    
    private double meanDistance(){
    	return sumDistances/distance.length;
    }
    
    private double sumDistancesEmotion(){
        double sumDistancesEmotion = 0.0;
        for(int i =1; i<distance.length; i++){
        	sumDistancesEmotion += distance[i];
        }    
        return sumDistancesEmotion;
    }
    
    private double meanDistanceEmotion(){
    	return sumDistancesEmotion()/(distance.length-1);
    }

	public double[] getKalmanPercentage() {
		return kalmanPercentage;
	}

	public void setKalmanPercentage(double[] kalmanPercentage) {
		for (int i = 0; i < kalmanPercentage.length; i++){
			this.kalmanPercentage[i] = kalmanPercentage[i];
		}
	}

}
