package emotionrecognition;

import static com.googlecode.javacv.cpp.opencv_contrib.createFisherFaceRecognizer;
import static com.googlecode.javacv.cpp.opencv_core.CV_32FC1;
import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
import static com.googlecode.javacv.cpp.opencv_core.cvCloneImage;
import static com.googlecode.javacv.cpp.opencv_core.cvGetCol;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSeqElem;
import static com.googlecode.javacv.cpp.opencv_core.cvTranspose;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvSaveImage;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import utilities.ImageUtilities;

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.cpp.opencv_contrib.FaceRecognizer;
import com.googlecode.javacv.cpp.opencv_core.CvMat;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_core.MatVector;

import facedetection.DetectFaces;

public class InstantEmotionRecognitionIO {
	
	static Logger logger = jade.util.Logger.getMyLogger("InstantEmotionRecognitionIO");
	private static final String IMAGE_FORMAT = ".png";
	private static final String EIGENFACES_PREFIX = "eigen_";
	private static final String FISHERFACES_FILE_PATH = "resources/emotion_data.txt";
	static DetectFaces faceDetector = new DetectFaces();

	public static boolean train(String csv_filename){
		int number_of_lines = number_of_lines(csv_filename);
		System.out.println(number_of_lines);
		MatVector emotionImages = new MatVector(number_of_lines);
		int[] labels = new int[number_of_lines];
		FaceRecognizer emotionClassifier = createFisherFaceRecognizer();
		try{
			read_csv(csv_filename, emotionImages, labels);
			emotionClassifier.train(emotionImages, labels);
			emotionClassifier.save(FISHERFACES_FILE_PATH);
		}
		catch(Exception e){
			return false;
		}
		return true;
	}
	
	public void load(FaceRecognizer emotionClassifier){
		emotionClassifier.load(FISHERFACES_FILE_PATH);
	}
	
	private static void read_csv(String csv_filename, MatVector images, int[] labels) {
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";
		IplImage load_image = null;
		CvRect faceRectangle = null;
		int counter = 0;
		IplImage grayImg;
		String[] image;
	 
		try {
	 
			br = new BufferedReader(new FileReader(csv_filename));
			while ((line = br.readLine()) != null) {
	 
			    // use dot comma as separator
				
				image = line.split(cvsSplitBy);
	 
				logger.log(jade.util.Logger.INFO,"Image [code= " + image[0] 
	                                 + " , emotion=" + image[1] + "]");
				
				load_image = cvLoadImage(image[0]);
				faceRectangle = detectFaceForTraining(load_image);
				load_image = cvCloneImage(ImageUtilities.cropAndResizeToFace(faceRectangle, load_image));
				grayImg = IplImage.create(load_image.width(), load_image.height(), IPL_DEPTH_8U, 1);
	            cvCvtColor(load_image, grayImg, CV_BGR2GRAY);
				images.put(counter, grayImg);
//				cvSaveImage("output/" + counter + ".jpg", grayImg);
//				load_image.release();
				labels[counter] = (Integer.parseInt(image[1]));
				counter++;
			}
	 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	 
		logger.log(jade.util.Logger.INFO,"Done");
	}
	
	public static CvRect detectFaceForTraining(IplImage image_to_detect){
		//Scale new grayscale frame if it is needed	
		CvRect Rectangle = new CvRect(cvGetSeqElem(faceDetector.detectFace(image_to_detect), 0));
		return Rectangle;
	}
	
	private static int number_of_lines(String csv_filename){
    	try{
    		 
    		File file =new File(csv_filename);
 
    		if(file.exists()){
 
    		    FileReader fr = new FileReader(file);
    		    LineNumberReader lnr = new LineNumberReader(fr);
 
    		    int linenumber = 0;
 
    	            while (lnr.readLine() != null){
    	        	linenumber++;
    	            }

    	            lnr.close();
    	            
    	            return linenumber;
 
 
    		}else{
    			 logger.log(jade.util.Logger.INFO,"File does not exists!");
    		}
 
    	}catch(IOException e){
    		e.printStackTrace();
    	}
    	return -1;
	}
	
	private static void saveMeanAndEigenfaces(FaceRecognizer emotionClassifier){
		// Mean image of the database
		CvMat mean = emotionClassifier.getMat("mean");
		IplImage mean_image = ImageUtilities.toReshapedAndNormalizedImage(mean);
		CanvasFrame canvasMean = new CanvasFrame("Mean");
		canvasMean.setDefaultCloseOperation(CanvasFrame.EXIT_ON_CLOSE);
		canvasMean.showImage(mean_image);

		Date now = new Date();
		String date = new SimpleDateFormat("dd-MM-yyyy-HH:mm:ss").format(now);
		makeDirectory("output/" + date);
		cvSaveImage("output/" + date + "/" + "mean" + IMAGE_FORMAT, mean_image);
		
		CvMat eigenvectors = emotionClassifier.getMat("eigenvectors");
		for (int i = 0; i<eigenvectors.cols(); i++){
			CvMat eigenvector = getEigenVector(i, eigenvectors);
			IplImage image = ImageUtilities.toReshapedAndNormalizedImage(eigenvector);
			cvSaveImage("output" + date + "/" + EIGENFACES_PREFIX + "image" + i + IMAGE_FORMAT, image);
		}		
	}
	
	private static void makeDirectory(String dir)
	// create a new directory or delete the contents of an existing one
	{
		File dirF = new File(dir);
		if (dirF.isDirectory()) {
			logger.log(jade.util.Logger.INFO,"Directory: " + dir + " already exists");
		}
		else {
			dirF.mkdir();
			logger.log(jade.util.Logger.INFO,"Created new directory: " + dir);
		}
	}  // end of makeDirectory()

    private static CvMat getEigenVector(int componentIndex, CvMat eigenVectors) {
        CvMat eigenVector = CvMat.createHeader(eigenVectors.rows(),
        										1,
        										CV_32FC1);
        cvGetCol(eigenVectors, eigenVector, componentIndex);
        CvMat tEigenVector = CvMat.create(eigenVector.cols(), eigenVector.rows(), eigenVector.depth(), eigenVector.channels());
        cvTranspose(eigenVector, tEigenVector);
        return tEigenVector;
    }
}
