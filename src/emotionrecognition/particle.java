package emotionrecognition;

import java.util.Date;

public class particle implements java.io.Serializable {
	private double[] position = new double[3];
	private double[] totalAttractorVelocity = new double[2];
	private double[] totalSlidingVelocity = new double[2];
    private long creation_timestamp = 0;
	
	// Atractors positions in DES
	private final double[] happyness_attractor = {60, 60, 0};
	private final double[] anger_attractor = {-60, 60, 0};
	private final double[] sadness_attractor = {-60, -60, 0};
	private final double[] fear_attractor = {60, -60, 0};
	
	// DES parameters for a paraboloid (x, y, ax^2+by^2)
	private static double a = 0.6;
	private static double b = 0.6;

	public particle(){
		
	}
	
	public double getA(){
		return a;
	}
	
	public double getB(){
		return b;
	}
	
	public double[] getPosition() {
		return position;
	}

	public void setPosition(double[] position) {
		this.position = position;
	}
	
	public double[][] getVelocity(double[] input_signal){
		double[][] velocity = new double[4][2];
		double[] scales = getScales();
		double[] radius = getRadius();
		
		//ANGER
		velocity[0][0] = Math.signum(anger_attractor[0]-position[0])*input_signal[0]/scales[0];
		velocity[0][1] = Math.signum(anger_attractor[1]-position[1])*input_signal[0]*radius[0]/scales[0];
		
		//FEAR
		velocity[1][0] = Math.signum(fear_attractor[0]-position[0])*input_signal[1]/scales[1];
		velocity[1][1] = Math.signum(fear_attractor[1]-position[1])*input_signal[1]*radius[1]/scales[1];
		
		//HAPPINESS
		velocity[2][0] = Math.signum(happyness_attractor[0]-position[0])*input_signal[2]/scales[2];
		velocity[2][1] = Math.signum(happyness_attractor[1]-position[1])*input_signal[2]*radius[2]/scales[2];
		
		//SADNESS
		velocity[3][0] = Math.signum(sadness_attractor[0]-position[0])*input_signal[3]/scales[3];
		velocity[3][1] = Math.signum(sadness_attractor[1]-position[1])*input_signal[3]*radius[3]/scales[3];
		
		return velocity;
	}
	
	public double[] getAttractorVelocity(double input_signal[]){
		double[][] velocity = getVelocity(input_signal);
		double[] attractor_velocity = new double[2];
		for( int i = 0; i < 4; i++){
			attractor_velocity[0] += velocity[i][0];
			attractor_velocity[1] += velocity[i][1];
		}
		return attractor_velocity;
	}
	
	public void updateTotalParticleAttractorVelocity(double input_signal[]){
		double[] attractor_velocity = getAttractorVelocity(input_signal);
		for(int i = 0; i < 2; i++){
			totalAttractorVelocity[i] = attractor_velocity[i];
		}
	}
	
	private double[] getScales(){
		double[] radius = getRadius();
		double[] scales = new double[4];
		for (int i = 0; i<4; i++){
			scales[i] = Math.sqrt(1+radius[i]*radius[i]+Math.pow((2*(a+b*radius[i]*radius[i])*position[0]),2));
		}
		return scales;
	}
	
	private double[] getRadius(){
		double[] radius = new double[4];
		radius[2] = Math.abs((happyness_attractor[1]-position[1])/(happyness_attractor[0]-position[0]));
		radius[0] = Math.abs((anger_attractor[1]-position[1])/(anger_attractor[0]-position[0]));
		radius[3] = Math.abs((sadness_attractor[1]-position[1])/(sadness_attractor[0]-position[0]));
		radius[1] = Math.abs((fear_attractor[1]-position[1])/(fear_attractor[0]-position[0]));
		return radius;
	}

	public double[] getTotalAttractorVelocity() {
		return totalAttractorVelocity;
	}

	public void setTotalAttractorVelocity(double[] totalAttractorVelocity) {
		this.totalAttractorVelocity = totalAttractorVelocity;
	}

	public double[] getTotalSlidingVelocity() {
		return totalSlidingVelocity;
	}

	public void setTotalSlidingVelocity(double[] totalSlidingVelocity) {
		this.totalSlidingVelocity = totalSlidingVelocity;
	}

	public long getCreationTimestamp() {
		return creation_timestamp;
	}

	public void setCreationTimestamp(long timestamp) {
		this.creation_timestamp = timestamp;
	}
}
