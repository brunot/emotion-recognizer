package userInterface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import static com.googlecode.javacv.cpp.opencv_core.*;
import agent.MonitorAgent;

import com.googlecode.javacv.cpp.opencv_core.IplImage;

import emotionrecognition.particle;
import emotionrecognition.recognizedEmotion;
import facedetection.detectedFace;
import facerecognition.recognizedPerson;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.showMessageDialog;


public class UserInterface extends javax.swing.JFrame {
	private MonitorAgent guiAgentInterface;

	public UserInterface(MonitorAgent agent) {
		guiAgentInterface = agent;
		initComponents();
		createParticleChart();
		createEmotionCharts();
		pack();
		this.setVisible(true);
	}

	public void selectImageAgentInput() {
		Object[] options = { "Input Webcam", "Input Arquivo" };
		int returnValue;
		String filename;

		int n = JOptionPane.showOptionDialog(this,
				"Qual o input para o programa?", "Escolha um tipo de input",
				JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
				options, options[0]);

		if (n == 0) {
			guiAgentInterface.selectImageAgentBehaviour("");
		} else if (n == 1) {
			JOptionPane
					.showMessageDialog(null,
							"Escolha o arquivo o vídeo de entrada.");
			// Pega o arquivo com pessoas para atualizar o banco atual
			fileChooser = new JFileChooser();
			returnValue = fileChooser.showOpenDialog(UserInterface.this);
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				filename = fileChooser.getSelectedFile().getAbsolutePath();
				// This is where a real application would open the file.
				guiAgentInterface.selectImageAgentBehaviour(filename);
			} else {
				JOptionPane
				.showMessageDialog(null,
						"Nenhum arquivo escolhido, encerrando o programa.");
				System.exit(1);
			}
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed"
	// desc="Generated Code">//GEN-BEGIN:initComponents
	private void initComponents() {

		jDialog1 = new javax.swing.JDialog();
		jInternalFrame1 = new javax.swing.JInternalFrame();
		jTabbedPane1 = new javax.swing.JTabbedPane();
		jCheckBox1 = new javax.swing.JCheckBox();
		jLabel10 = new javax.swing.JLabel();
		jLabel11 = new javax.swing.JLabel();
		jTabbedPane2 = new javax.swing.JTabbedPane();
		jPanel4 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jPanel1 = new javax.swing.JPanel();
		imageView = new javax.swing.JLabel();
		frameShot = new javax.swing.JButton();
		retrainEmotionButton = new javax.swing.JButton();
		startButton = new javax.swing.JButton();
		retrainPeople = new javax.swing.JButton();
		reset = new javax.swing.JButton();
		jPanel5 = new javax.swing.JPanel();
		GraphPanelHappyness = new javax.swing.JPanel();
		GraphPanelAnger = new javax.swing.JPanel();
		GraphPanelSadness = new javax.swing.JPanel();
		GraphPanelFear = new javax.swing.JPanel();
		jPanel6 = new javax.swing.JPanel();
		particlePanel = new javax.swing.JPanel();

		javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(
				jDialog1.getContentPane());
		jDialog1.getContentPane().setLayout(jDialog1Layout);
		jDialog1Layout.setHorizontalGroup(jDialog1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 400,
				Short.MAX_VALUE));
		jDialog1Layout.setVerticalGroup(jDialog1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 300,
				Short.MAX_VALUE));

		jInternalFrame1.setVisible(true);

		javax.swing.GroupLayout jInternalFrame1Layout = new javax.swing.GroupLayout(
				jInternalFrame1.getContentPane());
		jInternalFrame1.getContentPane().setLayout(jInternalFrame1Layout);
		jInternalFrame1Layout.setHorizontalGroup(jInternalFrame1Layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 0, Short.MAX_VALUE));
		jInternalFrame1Layout.setVerticalGroup(jInternalFrame1Layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 0, Short.MAX_VALUE));

		jCheckBox1.setText("jCheckBox1");

		jLabel10.setText("jLabel10");

		jLabel11.setText("jLabel11");

		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

		jLabel1.setText("Monitoração em Vídeo");

		jPanel1.setBorder(javax.swing.BorderFactory
				.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel1Layout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(imageView,
								javax.swing.GroupLayout.PREFERRED_SIZE, 640,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel1Layout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(imageView,
								javax.swing.GroupLayout.PREFERRED_SIZE, 480,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)));

		frameShot.setText("Capturar Tela");
		frameShot.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				frameShotActionPerformed(evt);
			}
		});

		retrainEmotionButton.setText("Retreinar Emoções");
		retrainEmotionButton
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						retrainEmotionButtonActionPerformed(evt);
					}
				});

		startButton.setText("Adicionar Nova Pessoa");
		startButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				startButtonActionPerformed(evt);
			}
		});

		retrainPeople.setText("Retreinar Pessoas");
		retrainPeople.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				retrainPeopleActionPerformed(evt);
			}
		});

		reset.setText("Resetar Avaliação da Emoção");
		reset.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				resetActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout
				.setHorizontalGroup(jPanel4Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel4Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel4Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel1)
																		.addGap(82,
																				82,
																				82))
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addComponent(
																				jPanel1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(18,
																				18,
																				18)
																		.addGroup(
																				jPanel4Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								startButton,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)
																						.addComponent(
																								frameShot,
																								javax.swing.GroupLayout.Alignment.TRAILING,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)
																						.addComponent(
																								retrainEmotionButton,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)
																						.addComponent(
																								retrainPeople,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)
																						.addComponent(
																								reset,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								248,
																								Short.MAX_VALUE))
																		.addContainerGap()))));
		jPanel4Layout
				.setVerticalGroup(jPanel4Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel4Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(jLabel1)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jPanel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(13, Short.MAX_VALUE))
						.addGroup(
								jPanel4Layout
										.createSequentialGroup()
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addComponent(reset)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(startButton)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(frameShot)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(retrainPeople)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(retrainEmotionButton)
										.addGap(42, 42, 42)));

		jTabbedPane2.addTab("Monitoração", jPanel4);

		GraphPanelHappyness.setLayout(new java.awt.BorderLayout());

		GraphPanelAnger.setLayout(new java.awt.BorderLayout());

		GraphPanelSadness.setLayout(new java.awt.BorderLayout());

		GraphPanelFear.setLayout(new java.awt.BorderLayout());

		javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(
				jPanel5);
		jPanel5.setLayout(jPanel5Layout);
		jPanel5Layout
				.setHorizontalGroup(jPanel5Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel5Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel5Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																GraphPanelAnger,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																GraphPanelHappyness,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addGroup(
												jPanel5Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																false)
														.addComponent(
																GraphPanelSadness,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addComponent(
																GraphPanelFear,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE))
										.addContainerGap()));

		jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { GraphPanelAnger, GraphPanelFear,
						GraphPanelHappyness, GraphPanelSadness });

		jPanel5Layout
				.setVerticalGroup(jPanel5Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel5Layout
										.createSequentialGroup()
										.addGap(20, 20, 20)
										.addGroup(
												jPanel5Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																false)
														.addComponent(
																GraphPanelSadness,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addComponent(
																GraphPanelHappyness,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE))
										.addGap(18, 18, 18)
										.addGroup(
												jPanel5Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																false)
														.addComponent(
																GraphPanelAnger,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addComponent(
																GraphPanelFear,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { GraphPanelAnger, GraphPanelFear,
						GraphPanelHappyness, GraphPanelSadness });

		jTabbedPane2.addTab("Gráficos de Emoções", jPanel5);

		particlePanel.setLayout(new java.awt.BorderLayout());

		javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(
				jPanel6);
		jPanel6.setLayout(jPanel6Layout);
		jPanel6Layout.setHorizontalGroup(jPanel6Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel6Layout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(particlePanel,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addContainerGap()));
		jPanel6Layout.setVerticalGroup(jPanel6Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel6Layout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(particlePanel,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addContainerGap()));

		jTabbedPane2.addTab("Movimento da Particula", jPanel6);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.PREFERRED_SIZE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup()
						.addGap(0, 11, Short.MAX_VALUE)
						.addComponent(jTabbedPane2,
								javax.swing.GroupLayout.PREFERRED_SIZE, 578,
								javax.swing.GroupLayout.PREFERRED_SIZE)));

		pack();
	}// </editor-fold>//GEN-END:initComponents

	@SuppressWarnings("unused")
	private void retrainEmotionButtonActionPerformed1(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton1ActionPerformed
		Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());
		String filename;
		int returnValue;

		logger.log(jade.util.Logger.INFO, "Start Emotion Training");
		java.util.Date date = new java.util.Date();
		// Pega o arquivo com pessoas para atualizar o banco atual
		fileChooser = new JFileChooser();
		returnValue = fileChooser.showOpenDialog(UserInterface.this);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			filename = fileChooser.getSelectedFile().getAbsolutePath();
			// This is where a real application would open the file.
			logger.log(jade.util.Logger.INFO, "Got filename: " + filename);
			guiAgentInterface.trainEmotionRecognition(filename);
		} else {
			logger.log(jade.util.Logger.INFO, "Open command cancelled by user.");
		}

	}// GEN-LAST:event_jButton1ActionPerformed

	private void frameShotActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_frameShotActionPerformed
		guiAgentInterface.takeScreenshot();
	}// GEN-LAST:event_frameShotActionPerformed

	private void retrainEmotionButtonActionPerformed(
			java.awt.event.ActionEvent evt) {// GEN-FIRST:event_retrainEmotionButtonActionPerformed
		Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());
		String filename;
		int returnValue;

		logger.log(jade.util.Logger.INFO, "Start Emotion Training");
		java.util.Date date = new java.util.Date();
		// Pega o arquivo com pessoas para atualizar o banco atual
		fileChooser = new JFileChooser();
		returnValue = fileChooser.showOpenDialog(UserInterface.this);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			filename = fileChooser.getSelectedFile().getAbsolutePath();
			// This is where a real application would open the file.
			logger.log(jade.util.Logger.INFO, "Got filename: " + filename);
			guiAgentInterface.trainEmotionRecognition(filename);
		} else {
			logger.log(jade.util.Logger.INFO, "Open command cancelled by user.");
		}
	}// GEN-LAST:event_retrainEmotionButtonActionPerformed

	private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_startButtonActionPerformed
		guiAgentInterface.trainPersonRecognition();
	}// GEN-LAST:event_startButtonActionPerformed

	private void resetActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_resetActionPerformed
		Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());

		int dialogButton = JOptionPane.YES_NO_OPTION;
		int dialogResult = JOptionPane.showConfirmDialog(this,
				"Deseja Resetar o Sistema?", "Reset", dialogButton);
		if (dialogResult == 0) {
			resetGraphs();
			guiAgentInterface.resetAll();
		} else {
			logger.log(jade.util.Logger.INFO, "Reset cancelled.");
		}

	}// GEN-LAST:event_resetActionPerformed

	private void retrainPeopleActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_retrainPeopleActionPerformed
		Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());
		String filename;
		int returnValue;

		logger.log(jade.util.Logger.INFO, "Start People Training");
		java.util.Date date = new java.util.Date();
		// Alerta para arquivo com 3 pessoas
		JOptionPane
				.showMessageDialog(null,
						"Escolha o arquivo de entrada para o retreinamento. Mínimo de 3 fotos");
		// Pega o arquivo com pessoas para atualizar o banco atual
		fileChooser = new JFileChooser();
		returnValue = fileChooser.showOpenDialog(UserInterface.this);
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			filename = fileChooser.getSelectedFile().getAbsolutePath();
			// This is where a real application would open the file.
			logger.log(jade.util.Logger.INFO, "Got filename: " + filename);
			guiAgentInterface.trainPersonRecognition(filename);
		} else {
			logger.log(jade.util.Logger.INFO, "Open command cancelled by user.");
		}
	}// GEN-LAST:event_retrainPeopleActionPerformed


	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JPanel GraphPanelAnger;
	private javax.swing.JPanel GraphPanelFear;
	private javax.swing.JPanel GraphPanelHappyness;
	private javax.swing.JPanel GraphPanelSadness;
	private javax.swing.JButton frameShot;
	private javax.swing.JLabel imageView;
	private javax.swing.JCheckBox jCheckBox1;
	private javax.swing.JDialog jDialog1;
	private javax.swing.JInternalFrame jInternalFrame1;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel10;
	private javax.swing.JLabel jLabel11;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JPanel jPanel5;
	private javax.swing.JPanel jPanel6;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JTabbedPane jTabbedPane2;
	private javax.swing.JPanel particlePanel;
	private javax.swing.JButton reset;
	private javax.swing.JButton retrainEmotionButton;
	private javax.swing.JButton retrainPeople;
	private javax.swing.JButton startButton;
	// End of variables declaration//GEN-END:variables

	// FileChooser
	private JFileChooser fileChooser;

	// Image to be displayed
	private IplImage current_image;

	// JFreeChart Series
	private ArrayList<XYSeries> particleSeries = new ArrayList<XYSeries>();
	private ArrayList<XYSeries> angerSeries = new ArrayList<XYSeries>();
	private ArrayList<XYSeries> fearSeries = new ArrayList<XYSeries>();
	private ArrayList<XYSeries> happynessSeries = new ArrayList<XYSeries>();
	private ArrayList<XYSeries> sadnessSeries = new ArrayList<XYSeries>();
	private ArrayList<XYSeries> kalmanAngerSeries = new ArrayList<XYSeries>();
	private ArrayList<XYSeries> kalmanFearSeries = new ArrayList<XYSeries>();
	private ArrayList<XYSeries> kalmanHappynessSeries = new ArrayList<XYSeries>();
	private ArrayList<XYSeries> kalmanSadnessSeries = new ArrayList<XYSeries>();

	private XYSeriesCollection particleDataset;
	
	private XYSeriesCollection angerDataset;
	private XYSeriesCollection fearDataset;
	private XYSeriesCollection happynessDataset;
	private XYSeriesCollection sadnessDataset;
	
	private int numberOfPeople = 0;
	private int numberOfPeopleParticle = 0;

	public JButton getStartButton() {
		return startButton;
	}

	public void setStartButton(JButton startButton) {
		this.startButton = startButton;
	}

	public void displayImage(IplImage image) {
		current_image = image;
		if (current_image != null) {
			if (current_image != null) {
				imageView.setIcon(new ImageIcon(current_image
						.getBufferedImage()));
			} else {
				showMessageDialog(UserInterface.this, "Image not opened",
						getTitle(), ERROR_MESSAGE);
			}
		}
	}

	public IplImage processDR(ArrayList<detectedFace> det,
			ArrayList<recognizedPerson> personRecognized,
			ArrayList<recognizedEmotion> emotionRecognized, IplImage frame) {
		int total = det.size();
		for (int i = 0; i < total; i++) {

			// desenha um retangulo na imagem em torno de cada deteccao
			cvRectangle(
					frame,
					cvPoint(det.get(i).getXinitialPoint(), det.get(i)
							.getYinitialPoint()),
					cvPoint(det.get(i).getXfinalPoint(), det.get(i)
							.getYfinalPoint()), CvScalar.GREEN, 2, CV_AA, 0);

			cvPutText(frame, personRecognized.get(i).getName(),
					new CvPoint(personRecognized.get(i).getXpontoNome(),
							personRecognized.get(i).getYpontoNome()),
					cvFont(2, 2), CvScalar.BLUE);

		}
		// cvSaveImage("teste.png", frame);
		return frame;
	}
	
	private void resetGraphs(){
		particleSeries.clear();
		angerSeries.clear();
		fearSeries.clear();
		happynessSeries.clear();
		sadnessSeries.clear();
		kalmanAngerSeries.clear();
		kalmanFearSeries.clear();
		kalmanHappynessSeries.clear();
		kalmanSadnessSeries.clear();

		particleDataset.removeAllSeries();
		
		angerDataset.removeAllSeries();
		fearDataset.removeAllSeries();
		happynessDataset.removeAllSeries();
		sadnessDataset.removeAllSeries();
		
		numberOfPeople = 0;
		numberOfPeopleParticle = 0;
	}
	
	private void createNewEmotionSeries() {
		numberOfPeople++;
		angerSeries.add(new XYSeries("Pessoa "+numberOfPeople+" Raiva"));
		angerDataset.addSeries(angerSeries.get(numberOfPeople - 1));
		fearSeries.add(new XYSeries("Pessoa "+numberOfPeople+" Medo"));
		fearDataset.addSeries(fearSeries.get(numberOfPeople - 1));
		happynessSeries.add(new XYSeries("Pessoa "+numberOfPeople+" Felicidade"));
		happynessDataset.addSeries(happynessSeries.get(numberOfPeople -1));
		sadnessSeries.add(new XYSeries("Pessoa "+numberOfPeople+" Tristeza"));
		sadnessDataset.addSeries(sadnessSeries.get(numberOfPeople - 1));
		kalmanAngerSeries.add(new XYSeries("Pessoa "+numberOfPeople+" Kalman Raiva"));
		angerDataset.addSeries(kalmanAngerSeries.get(numberOfPeople - 1));
		kalmanFearSeries.add(new XYSeries("Pessoa "+numberOfPeople+" Kalman Medo"));
		fearDataset.addSeries(kalmanFearSeries.get(numberOfPeople - 1));
		kalmanHappynessSeries.add(new XYSeries("Pessoa "+numberOfPeople+" Kalman Felicidade"));
		happynessDataset.addSeries(kalmanHappynessSeries.get(numberOfPeople -1));
		kalmanSadnessSeries.add(new XYSeries("Pessoa "+numberOfPeople+" Kalman Tristeza"));
		sadnessDataset.addSeries(kalmanSadnessSeries.get(numberOfPeople - 1));
	}

	public void createEmotionCharts() {
		// Add the series to your data set
		angerDataset = new XYSeriesCollection();
		fearDataset = new XYSeriesCollection();
		happynessDataset = new XYSeriesCollection();
		sadnessDataset = new XYSeriesCollection();
		
		createNewEmotionSeries();


		// Generate the graph
		JFreeChart angerChart = ChartFactory.createXYLineChart("Raiva", // Title
				"Tempo", // x-axis Label
				"Confiabilidade", // y-axis Label
				angerDataset, // Dataset
				PlotOrientation.VERTICAL, // Plot Orientation
				true, // Show Legend
				true, // Use tooltips
				false // Configure chart to generate URLs?
				);
		JFreeChart fearChart = ChartFactory.createXYLineChart("Medo", // Title
				"Tempo", // x-axis Label
				"Confiabilidade", // y-axis Label
				fearDataset, // Dataset
				PlotOrientation.VERTICAL, // Plot Orientation
				true, // Show Legend
				true, // Use tooltips
				false // Configure chart to generate URLs?
				);
		JFreeChart happynessChart = ChartFactory.createXYLineChart(
				"Felicidade", // Title
				"Tempo", // x-axis Label
				"Confiabilidade", // y-axis Label
				happynessDataset, // Dataset
				PlotOrientation.VERTICAL, // Plot Orientation
				true, // Show Legend
				true, // Use tooltips
				false // Configure chart to generate URLs?
				);
		JFreeChart sadnessChart = ChartFactory.createXYLineChart("Tristeza", // Title
				"Tempo", // x-axis Label
				"Confiabilidade", // y-axis Label
				sadnessDataset, // Dataset
				PlotOrientation.VERTICAL, // Plot Orientation
				true, // Show Legend
				true, // Use tooltips
				false // Configure chart to generate URLs?
				);

		XYPlot xyPlot = (XYPlot) angerChart.getPlot();
		xyPlot.setDomainCrosshairVisible(true);
		xyPlot.setRangeCrosshairVisible(true);

		ChartPanel chartPanelAnger = new ChartPanel(angerChart);
		ChartPanel chartPanelFear = new ChartPanel(fearChart);
		ChartPanel chartPanelHappyness = new ChartPanel(happynessChart);
		ChartPanel chartPanelSadness = new ChartPanel(sadnessChart);

		chartPanelAnger.setSize(new Dimension(GraphPanelHappyness.getWidth(),
				GraphPanelHappyness.getHeight()));
		chartPanelAnger.setPreferredSize(new Dimension(400, 240));
		chartPanelFear.setSize(new Dimension(GraphPanelSadness.getWidth(),
				GraphPanelSadness.getHeight()));
		chartPanelFear.setPreferredSize(new Dimension(400, 240));
		chartPanelHappyness.setSize(new Dimension(GraphPanelFear.getWidth(),
				GraphPanelFear.getHeight()));
		chartPanelHappyness.setPreferredSize(new Dimension(400, 240));
		chartPanelSadness.setSize(new Dimension(GraphPanelAnger.getWidth(),
				GraphPanelAnger.getHeight()));
		chartPanelSadness.setPreferredSize(new Dimension(400, 240));

		GraphPanelHappyness.add(chartPanelAnger, BorderLayout.CENTER);
		GraphPanelSadness.add(chartPanelFear, BorderLayout.CENTER);
		GraphPanelFear.add(chartPanelHappyness, BorderLayout.CENTER);
		GraphPanelAnger.add(chartPanelSadness, BorderLayout.CENTER);
	}

	public void refreshEmotionCharts(
			List<ArrayList<recognizedEmotion>> recognizedEmotionList) {
		Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());

		boolean isFirstIterator = false;
		long initialTime = 0;
		long time;
		
		while (recognizedEmotionList.size() > numberOfPeople){
			createNewEmotionSeries();
		}
		int i = 0;

		Iterator<ArrayList<recognizedEmotion>> emotionArrays = recognizedEmotionList.iterator();

		while(emotionArrays.hasNext()){
			Iterator<recognizedEmotion> emotionIterator = (Iterator<recognizedEmotion>) emotionArrays.next().iterator();
			angerSeries.get(i).clear();
			fearSeries.get(i).clear();
			happynessSeries.get(i).clear();
			sadnessSeries.get(i).clear();
			kalmanAngerSeries.get(i).clear();
			kalmanFearSeries.get(i).clear();
			kalmanHappynessSeries.get(i).clear();
			kalmanSadnessSeries.get(i).clear();
			while(emotionIterator.hasNext()){
				recognizedEmotion emotion = emotionIterator.next();
				if (!isFirstIterator) {
					initialTime = emotion.getCreationTimestamp();
					isFirstIterator = true;
				}
				time = emotion.getCreationTimestamp() - initialTime;
				angerSeries.get(i).add((double) (time) / 1000,
						emotion.getAllEmotionsBase100()[0]);
				fearSeries.get(i).add((double) (time) / 1000,
						emotion.getAllEmotionsBase100()[1]);
				happynessSeries.get(i).add((double) (time) / 1000,
						emotion.getAllEmotionsBase100()[2]);
				sadnessSeries.get(i).add((double) (time) / 1000,
						emotion.getAllEmotionsBase100()[3]);
				kalmanAngerSeries.get(i).add((double) (time) / 1000,
						emotion.getKalmanPercentage()[0]);
				kalmanFearSeries.get(i).add((double) (time) / 1000,
						emotion.getKalmanPercentage()[1]);
				kalmanHappynessSeries.get(i).add((double) (time) / 1000,
						emotion.getKalmanPercentage()[2]);
				kalmanSadnessSeries.get(i).add((double) (time) / 1000,
						emotion.getKalmanPercentage()[3]);
			}
			i++;
			emotionIterator = null;
			isFirstIterator = false;
		}
		
		emotionArrays = null;

		logger.log(jade.util.Logger.INFO, "Charts refreshed.");

	}

	private void createNewParticleSeries() {
		numberOfPeopleParticle++;
		particleSeries.add(new XYSeries("Particula " + numberOfPeopleParticle));
		particleDataset.addSeries(particleSeries.get(numberOfPeopleParticle-1));
	}
	
	public void createParticleChart() {
		particleDataset = new XYSeriesCollection();
		
		createNewParticleSeries();

		JFreeChart particleChart = ChartFactory.createScatterPlot("Partícula", // Title
				"X", // x-axis Label
				"Y", // y-axis Label
				particleDataset, // Dataset
				PlotOrientation.VERTICAL, // Plot Orientation
				true, // Show Legend
				true, // Use tooltips
				false // Configure chart to generate URLs?
				);

		XYPlot xyPlot = (XYPlot) particleChart.getPlot();
		xyPlot.setDomainCrosshairVisible(true);
		xyPlot.setRangeCrosshairVisible(true);

		XYItemRenderer renderer = xyPlot.getRenderer();
		renderer.setSeriesPaint(0, Color.blue);
		renderer.setSeriesPaint(1, Color.red);
		renderer.setSeriesPaint(2, Color.yellow);
		renderer.setSeriesPaint(3, Color.black);
		NumberAxis domain = (NumberAxis) xyPlot.getDomainAxis();
		domain.setRange(-5.00, 5.00);
		domain.setTickUnit(new NumberTickUnit(1));
		NumberAxis range = (NumberAxis) xyPlot.getRangeAxis();
		range.setRange(-5.0, 5.0);
		range.setTickUnit(new NumberTickUnit(1));

		ChartPanel chartPanelParticle = new ChartPanel(particleChart);
		particlePanel.add(chartPanelParticle, BorderLayout.CENTER);
	}

	public void refreshParticleChart(List<ArrayList<particle>> particleMotion) {
		Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());
		int i = 0;
		
		Iterator<ArrayList<particle>> particleArrays = particleMotion.iterator();
		while (particleMotion.size() > numberOfPeopleParticle){
			createNewParticleSeries();
		}
		
		while(particleArrays.hasNext()){
			Iterator<particle> particleIterator = (Iterator<emotionrecognition.particle>) particleArrays.next().iterator();
			particleSeries.get(i).clear();
			while(particleIterator.hasNext()){
				particle particle = particleIterator.next();
				particleSeries.get(i).add(particle.getPosition()[0], particle.getPosition()[1]);
			}
			i++;
			particleIterator = null;
		}
		
		particleArrays = null;

		logger.log(jade.util.Logger.INFO, "Particle refreshed.");
	}
}
