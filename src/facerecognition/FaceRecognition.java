/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facerecognition;

import com.googlecode.javacpp.FloatPointer;
import com.googlecode.javacpp.PointerPointer;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import facedetection.detectedFace;
import utilities.*;

import java.util.ArrayList;
import java.util.logging.Logger;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
import static com.googlecode.javacv.cpp.opencv_legacy.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;



/**
 * Recognizes faces.
 * 
 * @author reed
 */
public class FaceRecognition{

	/** the training face image array */
	IplImage[] trainingFaceImgArr;
	public FaceRecognitionIO faceRecognitionIO = new FaceRecognitionIO();
	Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());
	
	/** Constructs a new FaceRecognition instance with webcam */
	public FaceRecognition() {
		faceRecognitionIO.loadTrainingData();
	}


	/**
	 * Trains from the data in the given training text index file, and store the
	 * trained data into the file 'data/facedata.xml'.
	 * 
	 * @param trainingFileName
	 *            the given training text index file
	 */
	public void learn(final String trainingFileName) {
		int i;
		
		// load training data
		logger.log(jade.util.Logger.INFO,"===========================================");
		logger.log(jade.util.Logger.INFO,"Loading the training images in " + trainingFileName);
		trainingFaceImgArr = faceRecognitionIO.loadFaceImgArray(trainingFileName);
		faceRecognitionIO.nTrainFaces = trainingFaceImgArr.length;
		logger.log(jade.util.Logger.INFO,"Got " + faceRecognitionIO.nTrainFaces + " training images");
		if (faceRecognitionIO.nTrainFaces < 3) {
			logger.log(jade.util.Logger.INFO, "Need 3 or more training faces\n"
					+ "Input file contains only " + faceRecognitionIO.nTrainFaces);
			return;
		}

		// aplicar o DCT nas faces de treinamento
		doDCT();

		// do Principal Component Analysis on the training faces
		doPCA();

		System.out
				.println("projecting the training images onto the PCA subspace");
		// project the training images onto the PCA subspace
		faceRecognitionIO.projectedTrainFaceMat = cvCreateMat(faceRecognitionIO.nTrainFaces, // rows
				faceRecognitionIO.nEigens, // cols
				CV_32FC1); // type, 32-bit float, 1 channel

		// initialize the training face matrix - for ease of debugging
		for (int i1 = 0; i1 < faceRecognitionIO.nTrainFaces; i1++) {
			for (int j1 = 0; j1 < faceRecognitionIO.nEigens; j1++) {
				faceRecognitionIO.projectedTrainFaceMat.put(i1, j1, 0.0);
			}
		}

		logger.log(jade.util.Logger.INFO,"created projectedTrainFaceMat with " + faceRecognitionIO.nTrainFaces
				+ " (nTrainFaces) rows and " + faceRecognitionIO.nEigens + " (nEigens) columns");
		if (faceRecognitionIO.nTrainFaces < 5) {
			logger.log(jade.util.Logger.INFO,"projectedTrainFaceMat contents:\n"
					+ DataUtilities.oneChannelCvMatToString(faceRecognitionIO.projectedTrainFaceMat));
		}

		final FloatPointer floatPointer = new FloatPointer(faceRecognitionIO.nEigens);
		for (i = 0; i < faceRecognitionIO.nTrainFaces; i++) {
			cvEigenDecomposite(trainingFaceImgArr[i], // obj
					faceRecognitionIO.nEigens, // nEigObjs
					new PointerPointer(faceRecognitionIO.eigenVectArr), // eigInput (Pointer)
					0, // ioFlags
					null, // userData (Pointer)
					faceRecognitionIO.pAvgTrainImg, // avg
					floatPointer); // coeffs (FloatPointer)

			if (faceRecognitionIO.nTrainFaces < 5) {
				logger.log(jade.util.Logger.INFO,"floatPointer: "
						+ DataUtilities.floatPointerToString(floatPointer));
			}
			for (int j1 = 0; j1 < faceRecognitionIO.nEigens; j1++) {
				faceRecognitionIO.projectedTrainFaceMat.put(i, j1, floatPointer.get(j1));
			}
		}
		if (faceRecognitionIO.nTrainFaces < 5) {
			System.out
					.println("projectedTrainFaceMat after cvEigenDecomposite:\n"
							+ faceRecognitionIO.projectedTrainFaceMat);
		}

		// store the recognition data as an xml file
		faceRecognitionIO.storeTrainingData();

		// Save all the eigenvectors as images, so that they can be checked.
		faceRecognitionIO.storeEigenfaceImages();
	}

	/**
	 * Recognizes the face in each of the test images given, and compares the
	 * results with the truth.
	 * 
	 * @param szFileTest
	 *            the index file of test images
	 */
	public void recognizeFileList(final String szFileTest) {
		logger.log(jade.util.Logger.INFO,"===========================================");
		logger.log(jade.util.Logger.INFO,"recognizing faces indexed from " + szFileTest);
		int i = 0;
		int nTestFaces = 0; // the number of test images
		CvMat trainPersonNumMat; // the person numbers during training
		float[] projectedTestFace;
		String answer;
		int nCorrect = 0;
		int nWrong = 0;
		double timeFaceRecognizeStart;
		double tallyFaceRecognizeTime;
		float confidence = 0.0f;

		// load test images and ground truth for person number
		faceRecognitionIO.testFaceImgArr = faceRecognitionIO.loadFaceImgArray(szFileTest);
		nTestFaces = faceRecognitionIO.testFaceImgArr.length;

		logger.log(jade.util.Logger.INFO,nTestFaces + " test faces loaded");

		// load the saved training data
		if (faceRecognitionIO.pTrainPersonNumMat == null) {
			return;
		}

		// project the test images onto the PCA subspace
		projectedTestFace = new float[faceRecognitionIO.nEigens];
		timeFaceRecognizeStart = (double) cvGetTickCount(); // Record the
															// timing.

		for (i = 0; i < nTestFaces; i++) {
			int iNearest;
			int nearest;
			int truth;

			// project the test image onto the PCA subspace
			cvEigenDecomposite(faceRecognitionIO.testFaceImgArr[i], // obj
					faceRecognitionIO.nEigens, // nEigObjs
					new PointerPointer(faceRecognitionIO.eigenVectArr), // eigInput (Pointer)
					0, // ioFlags
					null, // userData
					faceRecognitionIO.pAvgTrainImg, // avg
					projectedTestFace); // coeffs

			// logger.log(jade.util.Logger.INFO,"projectedTestFace\n" +
			// floatArrayToString(projectedTestFace));

			final FloatPointer pConfidence = new FloatPointer(confidence);
			iNearest = findNearestNeighbor(projectedTestFace, new FloatPointer(
					pConfidence));
			confidence = pConfidence.get();
			truth = faceRecognitionIO.personNumTruthMat.data_i().get(i);
			nearest = faceRecognitionIO.pTrainPersonNumMat.data_i().get(iNearest);

			if (nearest == truth) {
				answer = "Correct";
				nCorrect++;
			} else {
				answer = "WRONG!";
				nWrong++;
			}
			logger.log(jade.util.Logger.INFO,"nearest = " + nearest + ", Truth = " + truth
					+ " (" + answer + "). Confidence = " + confidence);
		}
		tallyFaceRecognizeTime = (double) cvGetTickCount()
				- timeFaceRecognizeStart;
		if (nCorrect + nWrong > 0) {
			logger.log(jade.util.Logger.INFO,"TOTAL ACCURACY: "
					+ (nCorrect * 100 / (nCorrect + nWrong)) + "% out of "
					+ (nCorrect + nWrong) + " tests.");
			System.out
					.println("TOTAL TIME: "
							+ (tallyFaceRecognizeTime / (cvGetTickFrequency() * 1000.0 * (nCorrect + nWrong)))
							+ " ms average.");
		}
	}

	/**
	 * Recognizes the face in the test image given.
	 * 
	 * @param faceImage
	 *            the face image
	 */
	public recognizedPerson recognizeFace(IplImage faceImage) {
		logger.log(jade.util.Logger.INFO,"===========================================");
		logger.log(jade.util.Logger.INFO,"recognizing face");
//		CvMat trainPersonNumMat; // the person numbers during training
		float[] projectedTestFace;
		int nCorrect = 0;
		int nWrong = 0;
		double timeFaceRecognizeStart;
		double tallyFaceRecognizeTime;
		float confidence = 0.0f;

		// load test image and ground truth for person number
		// testFaceImgArr = loadFaceImgArray(szFileTest);

		// load the saved training data
		 if (faceRecognitionIO.pTrainPersonNumMat == null) {
		 	return null;
		 }

		// project the test images onto the PCA subspace
		projectedTestFace = new float[faceRecognitionIO.nEigens];
		timeFaceRecognizeStart = (double) cvGetTickCount(); // Record the
															// timing.

		// reconhecimento de face
		int iNearest;
		int nearest;
		String personName;

		// project the test image onto the PCA subspace
		cvEigenDecomposite(faceImage, // obj
				faceRecognitionIO.nEigens, // nEigObjs
				new PointerPointer(faceRecognitionIO.eigenVectArr), // eigInput (Pointer)
				0, // ioFlags
				null, // userData
				faceRecognitionIO.pAvgTrainImg, // avg
				projectedTestFace); // coeffs

		// logger.log(jade.util.Logger.INFO,"projectedTestFace\n" +
		// floatArrayToString(projectedTestFace));

		final FloatPointer pConfidence = new FloatPointer(confidence);
		iNearest = findNearestNeighbor(projectedTestFace, new FloatPointer(
				pConfidence));
		confidence = pConfidence.get();
		// truth = personNumTruthMat.data_i().get(i);
		nearest = faceRecognitionIO.pTrainPersonNumMat.data_i().get(iNearest);
		// Obs: nearest-1 para transformar numero de pessoa em indice do vetor
		personName = faceRecognitionIO.personNames.get(nearest - 1);

		logger.log(jade.util.Logger.INFO,"nearest = " + personName + ". Confidence = "
				+ confidence);
		tallyFaceRecognizeTime = (double) cvGetTickCount()
				- timeFaceRecognizeStart;
		if (nCorrect + nWrong > 0) {
			// logger.log(jade.util.Logger.INFO,"TOTAL ACCURACY: " + (nCorrect * 100 /
			// (nCorrect + nWrong)) + "% out of " + (nCorrect + nWrong) +
			// " tests.");
			System.out
					.println("TOTAL TIME: "
							+ (tallyFaceRecognizeTime / (cvGetTickFrequency() * 1000.0 * (nCorrect + nWrong)))
							+ " ms average.");
		}
		recognizedPerson person = new recognizedPerson();
		person.setName(personName);
		person.setName("Default");
		person.setConfidence(confidence);
		// trainPersonNumMat = null;
		// cvReleaseImage(trainPersonNumMat);
		// trainPersonNumMat.deallocate();
		return person;
	}

	public ArrayList<recognizedPerson> recognizeFaces(
			ArrayList<detectedFace> det, IplImage frame) {
		int total = det.size();
		ArrayList<recognizedPerson> rec = new ArrayList<recognizedPerson>();
		for (int i = 0; i < total; i++) {

			// copia para nova matriz e salva em arquivo
			CvRect r = new CvRect(det.get(i).getXinitialPoint(), det.get(i)
					.getYinitialPoint(), det.get(i).getXfinalPoint()
					- det.get(i).getXinitialPoint(), det.get(i)
					.getYfinalPoint() - det.get(i).getYinitialPoint());
			cvSetImageROI(frame, r);
			IplImage face = cvCreateImage(cvGetSize(frame), IPL_DEPTH_8U, 1);
			cvCvtColor(frame, face, CV_RGB2GRAY);
			IplImage faceR = cvCreateImage(cvSize(92, 112), face.depth(),
					face.nChannels());
			cvResize(face, faceR, CV_INTER_LINEAR);
			cvResetImageROI(frame);

			// Faz o reconhecimendo e imprime o nome da pessoa
			recognizedPerson person = new recognizedPerson();
			person = recognizeFace(faceR);
			// Ponto da impressao do nome da pessoa
			person.setXpontoNome(r.x());
			person.setYpontoNome(r.y() + r.height() + 40);
			person.setDetectedFace(det.get(i));
			rec.add(person);
			cvReleaseImage(face);
			cvReleaseImage(faceR);
			r.deallocate();
		}
		return rec;
	}

	/**
	 * Does the Principal Component Analysis, finding the average image and the
	 * eigenfaces that represent any image in the given dataset.
	 */
	private void doPCA() {
		int i;
		CvTermCriteria calcLimit;
		CvSize faceImgSize = new CvSize();

		// set the number of eigenvalues to use
		faceRecognitionIO.nEigens = faceRecognitionIO.nTrainFaces - 1;

		System.out
				.println("allocating images for principal component analysis, using "
						+ faceRecognitionIO.nEigens
						+ (faceRecognitionIO.nEigens == 1 ? " eigenvalue" : " eigenvalues"));

		// allocate the eigenvector images
		faceImgSize.width(trainingFaceImgArr[0].width());
		faceImgSize.height(trainingFaceImgArr[0].height());
		faceRecognitionIO.eigenVectArr = new IplImage[faceRecognitionIO.nEigens];
		for (i = 0; i < faceRecognitionIO.nEigens; i++) {
			faceRecognitionIO.eigenVectArr[i] = cvCreateImage(faceImgSize, // size
					IPL_DEPTH_32F, // depth
					1); // channels
		}

		// allocate the eigenvalue array
		faceRecognitionIO.eigenValMat = cvCreateMat(1, // rows
				faceRecognitionIO.nEigens, // cols
				CV_32FC1); // type, 32-bit float, 1 channel

		// allocate the averaged image
		faceRecognitionIO.pAvgTrainImg = cvCreateImage(faceImgSize, // size
				IPL_DEPTH_32F, // depth
				1); // channels

		// set the PCA termination criterion
		calcLimit = cvTermCriteria(CV_TERMCRIT_ITER, // type
				faceRecognitionIO.nEigens, // max_iter
				1); // epsilon

		System.out
				.println("computing average image, eigenvalues and eigenvectors");
		// compute average image, eigenvalues, and eigenvectors
		cvCalcEigenObjects(faceRecognitionIO.nTrainFaces, // nObjects
				new PointerPointer(trainingFaceImgArr), // input
				new PointerPointer(faceRecognitionIO.eigenVectArr), // output
				CV_EIGOBJ_NO_CALLBACK, // ioFlags
				0, // ioBufSize
				null, // userData
				calcLimit, faceRecognitionIO.pAvgTrainImg, // avg
				faceRecognitionIO.eigenValMat.data_fl()); // eigVals

		logger.log(jade.util.Logger.INFO,"normalizing the eigenvectors");
		cvNormalize(faceRecognitionIO.eigenValMat, // src (CvArr)
				faceRecognitionIO.eigenValMat, // dst (CvArr)
				1, // a
				0, // b
				CV_L1, // norm_type
				null); // mask
		// System.out.println(faceRecognitionIO.eigenValMat);
	}

	/**
	 * Does the Principal Component Analysis, finding the average image and the
	 * eigenfaces that represent any image in the given dataset.
	 */
	private void doDCT() {
		int iFace;
		CvMat img;
		IplImage img_flt;
		IplImage freq;
		int bsize = 16; // Indica o tamanho do bloco de frequencias de reconstrucao
		String fname;
		int y, x;

		for (iFace = 0; iFace < faceRecognitionIO.nTrainFaces; iFace++) {
			img = trainingFaceImgArr[iFace].asCvMat();

			// Converte a imagem original para Floating Point (Ponto Flutuante)
			img_flt = cvCreateImage(cvGetSize(img), IPL_DEPTH_32F, 1);
			cvConvert(img, img_flt);

			// Aplica a transformada DCT: imagem => frequencias
			freq = cvCreateImage(cvGetSize(img), IPL_DEPTH_32F, 1);
			cvDCT(img_flt, freq, CV_DXT_FORWARD);
			
			// Muda algumas frequencias para 0
			for (y = bsize; y < freq.asCvMat().rows(); y++) {
				for (x = bsize; x < freq.asCvMat().cols(); x++) {
					freq.asCvMat().put(y, x, 0.0);
				}
			}
			// Faz DCT inversa: (algumas baixas) frequencias => imagem
			cvDCT(freq, img_flt, CV_DXT_INVERSE);

			// Salva em arquivo a imagem reconstruida
			fname = faceRecognitionIO.saveImage("resources/DCT/idct_%04d_%d.png", bsize, img_flt, iFace);

			// Carrega o arquivo salvo para o trainingFaceImgArr[iFace]
			trainingFaceImgArr[iFace] = cvLoadImage(fname,
					CV_LOAD_IMAGE_GRAYSCALE);

			// Salva a imagem em trainingFaceImgArr[iFace] - n funciona
			// trainingFaceImgArr[iFace] = img_flt;
			// cvReleaseImage(img.asIplImage());
			img.release();
			cvReleaseImage(img_flt);
			cvReleaseImage(freq);
		}
	}

	/**
	 * Find the most likely person based on a detection. Returns the index, and
	 * stores the confidence value into pConfidence.
	 * 
	 * @param projectedTestFace
	 *            the projected test face
	 * @param pConfidencePointer
	 *            a pointer containing the confidence value
	 * @param iTestFace
	 *            the test face index
	 * @return the index
	 */
	private int findNearestNeighbor(float projectedTestFace[],
			FloatPointer pConfidencePointer) {
		double leastDistSq = Double.MAX_VALUE;
		int i = 0;
		int iTrain = 0;
		int iNearest = 0;

		logger.log(jade.util.Logger.INFO,"................");
		logger.log(jade.util.Logger.INFO,"find nearest neighbor from " + faceRecognitionIO.nTrainFaces
				+ " training faces");
		for (iTrain = 0; iTrain < faceRecognitionIO.nTrainFaces; iTrain++) {
			double distSq = 0;

			for (i = 0; i < faceRecognitionIO.nEigens; i++) {

				float projectedTrainFaceDistance = (float) faceRecognitionIO.projectedTrainFaceMat
						.get(iTrain, i);
				float d_i = projectedTestFace[i] - projectedTrainFaceDistance;
				distSq += d_i * d_i; // / eigenValMat.data_fl().get(i); //
										// Mahalanobis distance (might give
										// better results than Eucalidean
										// distance)
				// if (iTrain < 5) {
				// logger.log(jade.util.Logger.INFO,"    ** projected training face " +
				// (iTrain + 1) + " distance from eigenface " + (i + 1) + " is "
				// + projectedTrainFaceDistance);
				// logger.log(jade.util.Logger.INFO,"    distance between them " + d_i);
				// logger.log(jade.util.Logger.INFO,"    distance squared " + distSq);
				// }
			}

			if (distSq < leastDistSq) {
				leastDistSq = distSq;
				iNearest = iTrain;
				// logger.log(jade.util.Logger.INFO,"  training face " + (iTrain + 1)
				// 		+ " is the new best match, least squared distance: "
				// 		+ leastDistSq);
			}
		}

		// Return the confidence level based on the Euclidean distance,
		// so that similar images should give a confidence between 0.5 to 1.0,
		// and very different images should give a confidence between 0.0 to
		// 0.5.
		float pConfidence = (float) (1.0f - Math.sqrt(leastDistSq
				/ (float) (faceRecognitionIO.nTrainFaces * faceRecognitionIO.nEigens)) / 255.0f);
		pConfidencePointer.put(pConfidence);

		logger.log(jade.util.Logger.INFO,"training face " + (iNearest + 1)
				+ " is the final best match, confidence " + pConfidence);
		return iNearest;
	}

}
