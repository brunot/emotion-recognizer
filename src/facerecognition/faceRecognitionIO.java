package facerecognition;


import com.googlecode.javacpp.Pointer;
import com.googlecode.javacv.cpp.opencv_core.CvMat;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Logger;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import utilities.DataUtilities;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;

public class FaceRecognitionIO {

	/** the number of training faces */
	protected int nTrainFaces = 0;

	/** the test face image array */
	IplImage[] testFaceImgArr;
	/** the person number array */
	CvMat personNumTruthMat;
	
	CvMat pTrainPersonNumMat = null; // the person numbers during training
    Logger logger = jade.util.Logger.getMyLogger(this.getClass().getName());

	/** the person names */
	final ArrayList<String> personNames = new ArrayList<String>();
	/** the number of eigenvalues */
	int nEigens = 0;
	/** eigenvectors */
	IplImage[] eigenVectArr;
	/** eigenvalues */
	CvMat eigenValMat;
	/** the average image */
	IplImage pAvgTrainImg;
	/** the projected training faces */
	CvMat projectedTrainFaceMat;
	// Ativa impressao do nome na foto, reconhecimento dentro da funcao de
	// deteccao
	public boolean recognize = false;
	// Ativa salvar imagens para treino
	protected boolean saveForTrain = false;

	/** the number of persons */
	int nPersons;

	public void setnPersons(int nPersons) {
		this.nPersons = nPersons;
	}

	public int getnPersons() {
		return nPersons;
	}
	
	/** Stores the training data to the file 'resources/facedata.xml'. */
	public void storeTrainingData() {
		CvFileStorage fileStorage;
		int i;
		
		logger.log(jade.util.Logger.INFO,"writing resources/facedata.xml");

		// create a file-storage interface
		fileStorage = cvOpenFileStorage("resources/facedata.xml", // filename
				null, // memstorage
				CV_STORAGE_WRITE, // flags
				null); // encoding

		// Store the person names. Added by Shervin.
		cvWriteInt(fileStorage, // fs
				"nPersons", // name
				nPersons); // value

		for (i = 0; i < nPersons; i++) {
			String varname = "personName_" + (i + 1);
			cvWriteString(fileStorage, // fs
					varname, // name
					personNames.get(i), // string
					0); // quote
		}

		// store all the data
		cvWriteInt(fileStorage, // fs
				"nEigens", // name
				nEigens); // value

		cvWriteInt(fileStorage, // fs
				"nTrainFaces", // name
				nTrainFaces); // value

		cvWrite(fileStorage, // fs
				"trainPersonNumMat", // name
				personNumTruthMat, // value
				cvAttrList()); // attributes

		cvWrite(fileStorage, // fs
				"eigenValMat", // name
				eigenValMat, // value
				cvAttrList()); // attributes

		cvWrite(fileStorage, // fs
				"projectedTrainFaceMat", // name
				projectedTrainFaceMat, cvAttrList()); // value

		cvWrite(fileStorage, // fs
				"avgTrainImg", // name
				pAvgTrainImg, // value
				cvAttrList()); // attributes

		for (i = 0; i < nEigens; i++) {
			String varname = "eigenVect_" + i;
			cvWrite(fileStorage, // fs
					varname, // name
					eigenVectArr[i], // value
					cvAttrList()); // attributes
		}

		// release the file-storage interface
		cvReleaseFileStorage(fileStorage);
	}


	/** Saves all the eigenvectors as images, so that they can be checked. */
	public void storeEigenfaceImages() {
		// Store the average image to a file
		System.out
				.println("Saving the image of the average face as 'data/out_averageImage.bmp'");
		cvSaveImage("data/out_averageImage.bmp", pAvgTrainImg);

		// Create a large image made of many eigenface images.
		// Must also convert each eigenface image to a normal 8-bit UCHAR image
		// instead of a 32-bit float image.
		logger.log(jade.util.Logger.INFO,"Saving the " + nEigens
				+ " eigenvector images as 'data/out_eigenfaces.bmp'");

		if (nEigens > 0) {
			// Put all the eigenfaces next to each other.
			int COLUMNS = 8; // Put upto 8 images on a row.
			int nCols = Math.min(nEigens, COLUMNS);
			int nRows = 1 + (nEigens / COLUMNS); // Put the rest on new rows.
			int w = eigenVectArr[0].width();
			int h = eigenVectArr[0].height();
			CvSize size = cvSize(nCols * w, nRows * h);
			final IplImage bigImg = cvCreateImage(size, IPL_DEPTH_8U, // depth,
																		// 8-bit
																		// Greyscale
																		// UCHAR
																		// image
					1); // channels
			for (int i = 0; i < nEigens; i++) {
				// Get the eigenface image.
				IplImage byteImg = DataUtilities.convertFloatImageToUcharImage(eigenVectArr[i]);
				// Paste it into the correct position.
				int x = w * (i % COLUMNS);
				int y = h * (i / COLUMNS);
				CvRect ROI = cvRect(x, y, w, h);
				cvSetImageROI(bigImg, // image
						ROI); // rect
				cvCopy(byteImg, // src
						bigImg, // dst
						null); // mask
				cvResetImageROI(bigImg);
				cvReleaseImage(byteImg);
			}
			cvSaveImage("data/out_eigenfaces.bmp", // filename
					bigImg); // image
			cvReleaseImage(bigImg);
		}
	}

	/**
	 * Opens the training data from the file 'resources/facedata.xml'.
	 * 
	 * @param pTrainPersonNumMat
	 * @return the person numbers during training, or null if not successful
	 */
	public boolean loadTrainingData() {
		logger.log(jade.util.Logger.INFO,"loading training data");
		CvFileStorage fileStorage;
		int i;

		// create a file-storage interface
		fileStorage = cvOpenFileStorage("resources/facedata.xml", // filename
				null, // memstorage
				CV_STORAGE_READ, // flags
				null); // encoding
		if (fileStorage == null) {
			System.err
					.println("Can't open training database file 'resources/facedata.xml'.");
			return false;
		}

		// Load the person names.
		personNames.clear(); // Make sure it starts as empty.
		nPersons = cvReadIntByName(fileStorage, // fs
				null, // map
				"nPersons", // name
				0); // default_value
		if (nPersons == 0) {
			System.err
					.println("No people found in the training database 'resources/facedata.xml'.");
			return false;
		} else {
			logger.log(jade.util.Logger.INFO,nPersons
					+ " persons read from the training database");
		}

		// Load each person's name.
		for (i = 0; i < nPersons; i++) {
			String sPersonName;
			String varname = "personName_" + (i + 1);
			sPersonName = cvReadStringByName(fileStorage, // fs
					null, // map
					varname, "");
			personNames.add(sPersonName);
		}
		logger.log(jade.util.Logger.INFO,"person names: " + personNames);

		// Load the data
		nEigens = cvReadIntByName(fileStorage, // fs
				null, // map
				"nEigens", 0); // default_value
		nTrainFaces = cvReadIntByName(fileStorage, null, // map
				"nTrainFaces", 0); // default_value
		Pointer pointer = cvReadByName(fileStorage, // fs
				null, // map
				"trainPersonNumMat", // name
				cvAttrList()); // attributes
		pTrainPersonNumMat = new CvMat(pointer);

		pointer = cvReadByName(fileStorage, // fs
				null, // map
				"eigenValMat", // nmae
				cvAttrList()); // attributes
		eigenValMat = new CvMat(pointer);

		pointer = cvReadByName(fileStorage, // fs
				null, // map
				"projectedTrainFaceMat", // name
				cvAttrList()); // attributes
		projectedTrainFaceMat = new CvMat(pointer);

		pointer = cvReadByName(fileStorage, null, // map
				"avgTrainImg", cvAttrList()); // attributes
		pAvgTrainImg = new IplImage(pointer);

		eigenVectArr = new IplImage[nTrainFaces];
		for (i = 0; i < nEigens; i++) {
			String varname = "eigenVect_" + i;
			pointer = cvReadByName(fileStorage, null, // map
					varname, cvAttrList()); // attributes
			eigenVectArr[i] = new IplImage(pointer);
		}

		// release the file-storage interface
		cvReleaseFileStorage(fileStorage);
		pointer.setNull();

		logger.log(jade.util.Logger.INFO,"Training data loaded (" + nTrainFaces
				+ " training images of " + nPersons + " people)");
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("People: ");
		if (nPersons > 0) {
			stringBuilder.append("<").append(personNames.get(0)).append(">");
		}
		for (i = 1; i < nPersons; i++) {
			stringBuilder.append(", <").append(personNames.get(i)).append(">");
		}
		logger.log(jade.util.Logger.INFO,stringBuilder.toString());
		return true;
	}

	public boolean checkTrainingData(){
		// create a file-storage interface
		CvFileStorage fileStorage;
		fileStorage = cvOpenFileStorage("resources/facedata.xml", // filename
				null, // memstorage
				CV_STORAGE_READ, // flags
				null); // encoding
		if (fileStorage == null) {
			System.err
					.println("Can't open training database file 'resources/facedata.xml'.");
			return false;
		}
		cvReleaseFileStorage(fileStorage);
		return true;
	}


	/**
	 * Reads the names & image filenames of people from a text file, and loads
	 * all those images listed.
	 * 
	 * @param filename
	 *            the training file name
	 * @return the face image array
	 */
	public IplImage[] loadFaceImgArray(final String filename) {
		IplImage[] faceImgArr;
		BufferedReader imgListFile;
		String imgFilename;
		int iFace = 0;
		int nFaces = 0;
		int i;
		try {
			// open the input file
			imgListFile = new BufferedReader(new FileReader(filename));

			// count the number of faces
			while (true) {
				final String line = imgListFile.readLine();
				if (line == null || line.isEmpty()) {
					break;
				}
				nFaces++;
			}
			logger.log(jade.util.Logger.INFO,"nFaces: " + nFaces);
			imgListFile = new BufferedReader(new FileReader(filename));

			// allocate the face-image array and person number matrix
			faceImgArr = new IplImage[nFaces];
			personNumTruthMat = cvCreateMat(1, // rows
					nFaces, // cols
					CV_32SC1); // type, 32-bit unsigned, one channel

			// initialize the person number matrix - for ease of debugging
			for (int j1 = 0; j1 < nFaces; j1++) {
				personNumTruthMat.put(0, j1, 0);
			}

			personNames.clear(); // Make sure it starts as empty.
			nPersons = 0;

			// store the face images in an array
			for (iFace = 0; iFace < nFaces; iFace++) {
				String personName;
				String sPersonName;
				int personNumber;

				// read person number (beginning with 1), their name and the
				// image filename.
				final String line = imgListFile.readLine();
				if (line.isEmpty()) {
					break;
				}
				final String[] tokens = line.split(" ");
				personNumber = Integer.parseInt(tokens[0]);
				personName = tokens[1];
				imgFilename = "data/" + tokens[2];
				sPersonName = personName;
				logger.log(jade.util.Logger.INFO,"Got " + iFace + " " + personNumber + " "
						+ personName + " " + imgFilename);

				// Check if a new person is being loaded.
				if (personNumber > nPersons) {
					// Allocate memory for the extra person (or possibly
					// multiple), using this new person's name.
					personNames.add(sPersonName);
					nPersons = personNumber;
					logger.log(jade.util.Logger.INFO,"Got new person " + sPersonName
							+ " -> nPersons = " + nPersons + " ["
							+ personNames.size() + "]");
				}

				// Keep the data
				personNumTruthMat.put(0, // i
						iFace, // j
						personNumber); // v

				// load the face image
				faceImgArr[iFace] = cvLoadImage(imgFilename, // filename
						CV_LOAD_IMAGE_GRAYSCALE); // isColor

				if (faceImgArr[iFace] == null) {
					throw new RuntimeException("Can't load image from " + imgFilename);
				}
			}

			imgListFile.close();

		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}

		logger.log(jade.util.Logger.INFO,"Data loaded from '" + filename + "': (" + nFaces
				+ " images of " + nPersons + " people).");
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("People: ");
		if (nPersons > 0) {
			stringBuilder.append("<").append(personNames.get(0)).append(">");
		}
		for (i = 1; i < nPersons && i < personNames.size(); i++) {
			stringBuilder.append(", <").append(personNames.get(i)).append(">");
		}
		logger.log(jade.util.Logger.INFO,stringBuilder.toString());

		return faceImgArr;
	}

public String saveImage(String save_path, int size, IplImage image, int complement){
		String saved_image_path;
		saved_image_path = String.format(save_path, size, complement);
		cvSaveImage(saved_image_path, image);
		return saved_image_path;
	}
}
