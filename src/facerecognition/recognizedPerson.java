/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package facerecognition;

import com.googlecode.javacv.cpp.opencv_core.CvPoint;

import facedetection.detectedFace;

/**
 *
 * @author Rafael
 */
public class recognizedPerson implements java.io.Serializable {
    
    private float confidence;
    private String name;
    private int XpontoNome;
    private int YpontoNome;
    private detectedFace detectedFace;

    public int getXpontoNome() {
        return XpontoNome;
    }

    public void setXpontoNome(int XpontoNome) {
        this.XpontoNome = XpontoNome;
    }

    public int getYpontoNome() {
        return YpontoNome;
    }

    public void setYpontoNome(int YpontoNome) {
        this.YpontoNome = YpontoNome;
    }

    public float getConfidence() {
        return confidence;
    }

    public void setConfidence(float confidence) {
        this.confidence = confidence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public detectedFace getDetectedFace() {
		return detectedFace;
	}

	public void setDetectedFace(detectedFace detectedFace) {
		this.detectedFace = detectedFace;
	}    
}
