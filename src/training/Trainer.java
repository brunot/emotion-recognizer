package training;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;

import utilities.CameraGrabber;

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.cpp.opencv_core.CvRect;
import com.googlecode.javacv.cpp.opencv_core.CvSeq;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

import facedetection.DetectFaces;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
import static com.googlecode.javacv.cpp.opencv_legacy.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;

public class Trainer {
	// interface de acesso de memoria
	static CvSeq personFaces = null;
    static Logger logger = jade.util.Logger.getMyLogger("Trainer");
    
    public static boolean saveDetections(String name, CameraGrabber cameraGrabber, DetectFaces faceDetector){
    	int aux = 0;
    	IplImage frame = null;
    	for(int at = 0; at < 100; at++){
    		frame = cameraGrabber.grabFrameFromCamera();
    		personFaces = faceDetector.detectFace(frame);
    		aux = saveDetectionsName(aux, frame, name);
//    		cvReleaseImage(frame);
    	}
    	return true;
    }

	public static void agentTraining(String name, int number) {
		saveTrainingInTxt(number, name);
		logger.log(jade.util.Logger.INFO,"-----------New files saved to system-------------");
	}

	private static void saveTrainingInTxt(int personNumber, String name) {
		int loaded_images = 0;
		Random generator = new Random();
		ArrayList<Integer> list = new ArrayList<Integer>();
		while (loaded_images < 10) {
			int num_face = generator.nextInt(100);
			String recname = String.format("/rec_%04d.png", num_face);
			String path = "data/person/" + name + recname;
			if (checkImage(path)) {
				if (!list.contains(num_face)) {
					list.add(num_face);
					BufferedWriter writer = null;
					try {
						// create a temporary file
						String filename = "resources/train_agents.txt";
						File trainFile = new File(filename);

						writer = new BufferedWriter(new FileWriter(trainFile,
								true));
						writer.write("\r\n" + personNumber + " " + name
								+ " person/" + name + recname);
					} catch (Exception e) {
						e.printStackTrace();
					} finally {
						try {
							// Close the writer regardless of what happens...
							writer.close();
						} catch (Exception e) {
						}
					}
					loaded_images++;
				}
			}
		}
	}

	public static int saveDetectionsName(int num_face, IplImage img, String name) {
		int total = personFaces.total();
		CvRect r = null;
		IplImage img2 = null, imgR = null;
		String fname = null;
		File f = null;
		for (int i = 0; i < total; i++) {
			num_face = num_face + 1;

			// desenha um retangulo em torno de cada deteccao
//			
			r = new CvRect(cvGetSeqElem(personFaces, i));

			// copia para nova matriz e salva em arquivo
			cvSetImageROI(img, r);
			img2 = cvCreateImage(cvGetSize(img), img.depth(),
					img.nChannels());
			cvCopy(img, img2, null);
			cvResetImageROI(img);

			// desenha retangulo na imagem
			cvRectangle(img, cvPoint(r.x(), r.y()),
					cvPoint(r.x() + r.width(), r.y() + r.height()),
					CvScalar.GREEN, 2, CV_AA, 0);

			// Salva em arquivo a imagem redimensionada
			imgR = cvCreateImage(cvSize(92, 112), img2.depth(),
					img2.nChannels());
			cvResize(img2, imgR, CV_INTER_LINEAR);
			f = new File("data/person/" + name);
			try {
				f.mkdir();
			} catch (Exception e) {
				e.printStackTrace();
			}
			fname = String.format("data/person/" + name + "/rec_%04d.png",
					num_face);
			
			// fname = String.format("teste_%04d.png", num_face);
			cvSaveImage(fname, imgR);
			cvReleaseImage(img2);
			cvReleaseImage(imgR);
		}
//		canvas.showImage(img);
		return num_face;
	}

	private static boolean checkImage(String path) {
		IplImage image = cvLoadImage(path);
		if (image != null) {
			cvReleaseImage(image);
			return true;
		}
		return false;
	}
}
