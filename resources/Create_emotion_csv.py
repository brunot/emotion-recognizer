#!/usr/bin/env python

import sys
import os.path

if __name__ == "__main__":
    print (len(sys.argv))

    if len(sys.argv) != 2:
        print ("usage: create_csv <base_path>")
        sys.exit(1)

    if len(sys.argv) == 2:
        print ("ok")

    BASE_PATH=sys.argv[1]
    SEPARATOR=";"
    value = ""
    abs_file_path = BASE_PATH + '\emotions.csv'
    target = open('emotions.csv' , 'w+')

    for dirname, dirnames, filenames in os.walk(BASE_PATH):
        for subdirname in dirnames:
            subject_path = os.path.join(dirname, subdirname)
            for filename in os.listdir(subject_path):
                if filename.endswith(".png"):
                    abs_path = "%s\%s" % (subject_path, filename)
                    
                    print ("%s%s%d" % (abs_path, SEPARATOR, int(subdirname)))
                    target.write("%s%s%d" % (abs_path, SEPARATOR, int(subdirname)))
                    target.write("\n")
    target.close