Emotion Recognition Software
========================================

Introduction
=======================

This project is the final project for the mecathronics engineering course of the Politechnic School Engineering of the University of São Paulo made by Bruno Tinen with the orientation and help of prof. dr. Marcos Ribeiro Pereira-Barretto.

It is a JAVA based software, implemented using [JADE 4.3.1](http://jade.tilab.com/) and [OpenCV 2.4.8](http://opencv.org/). For more info and for downloading the softwares visit the respective websites.

This software is able to detect and recognize people as well as classify the emotion they reveal. It is capable of recognize happiness, fear, anger and sadness.

Installation
=======================
First of all clone this repository using https://brunot@bitbucket.org/brunot/tcc-jade.git.

All the required libraries are within the lib/ folder.

Setup
=====
A simple way to test this software and run it along with JADE is to set the run configurations as following

JADE main

![Image of Yaktocat](http://s22.postimg.org/ubgvkbkxd/jade_eclipse_main.jpg)

JADE arguments

![Image of Yaktocat](http://s30.postimg.org/tfz6u4tvl/jade_eclipse_arguments.jpg)

With the following arguments:

```
-gui ImageAgent:agent.ImageAgent;DetectionAgent:agent.DetectionAgent;MonitorAgent:agent.MonitorAgent;PersonRecognitionAgent:agent.PersonRecognitionAgent;InstantEmotionRecognitionAgent:agent.InstantEmotionRecognitionAgent;TrainPersonRecognitionAgent:agent.TrainPersonRecognitionAgent
```
